--Таблица out_city_trip
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)

DROP TABLE IF EXISTS places; 
CREATE TABLE places (
id VARCHAR(15) NOT NULL PRIMARY KEY,
region VARCHAR(15) REFERENCES regions (id) ON DELETE SET NULL,
city VARCHAR(15) REFERENCES cities (id) ON DELETE SET NULL,
addr VARCHAR(15) NOT NULL,
porch INTEGER DEFAULT 0,
lat REAL,
lon REAL
);
--Последовательность для таблицы: out_city_trip
DROP SEQUENCE IF EXISTS places_id_seq;
CREATE SEQUENCE places_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Добавление междугороднего заказа
DROP FUNCTION IF EXISTS f_new_place(_region VARCHAR(15),_city VARCHAR(15),_addr VARCHAR(15),_porch INTEGER,_lat REAL,_lon REAL);
CREATE OR REPLACE FUNCTION f_new_place(_region VARCHAR(15),_city VARCHAR(15),_addr VARCHAR(15),_porch INTEGER,_lat REAL,_lon REAL) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(252);
_id_city VARCHAR(15);
_id_region VARCHAR(15);
BEGIN
SELECT id FROM regions WHERE region = _region INTO _id_region;
SELECT id FROM cities WHERE city = _city INTO _id_city;
_id = 'PL';
SELECT nextval('places_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO places(id,region,city,addr,porch,lat,lon) VALUES(_id,_id_region,_id_city,_addr,_porch,_lat,_lon);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_place('ХМАО-Югра','Нижневартовск','Чапаева, 7',3,60.92994,76.58643);

--Таблица in_city_trip
--Статусы поездки:
--...
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"client" - индентификатор пользователя
--"driver" - индентификатор пользователя
--"places" - адреса отправления
--"out_city" - междугородний заказ 
--"plane" - запланированный заказ 
--"date_plane" - датапланирования
--"status_trip" - статус поезки
--"date_start" - дата начала заказа
--"date_end" - дата завершения заказа 
--"rating_client" - отзыв водителя о клиенте
--"rating_driver" - отзыв клиента о водителе
--"babych" - детское кресло
--"smoking" - курение 
--"fastup" - бстрая подача
--"animal" - животные
--"comment_exist" - существование комментария  
--"comment" - комментарии
--"pay" - оплата true-сбербанк онлайн, false-наличными
DROP TABLE IF EXISTS trip_client;
CREATE TABLE trip_client (
id VARCHAR(15) NOT NULL PRIMARY KEY,
client VARCHAR(15) REFERENCES users (id) ON DELETE SET NULL,
driver VARCHAR(15),
price INTEGER,
places BYTEA,
in_city BOOLEAN DEFAULT FALSE,
city VARCHAR(15),
srv VARCHAR(15),
duration_waiting INTEGER DEFAULT 180,
plane BOOLEAN DEFAULT FALSE,
date_plane BIGINT,
status_trip VARCHAR(15) NOT NULL,
date_start BIGINT NOT NULL,
date_end BIGINT,
rating_client BOOLEAN DEFAULT TRUE,
--cancel_client VARCHAR(15),
rating_driver BOOLEAN DEFAULT TRUE,
--cancel_driver VARCHAR(15),
cancel VARCHAR(15),
babych BOOLEAN DEFAULT FALSE,
smoking BOOLEAN DEFAULT FALSE,
fastup BOOLEAN DEFAULT FALSE,
animal BOOLEAN DEFAULT FALSE,
comment_exist BOOLEAN DEFAULT FALSE,
comment VARCHAR(255),
pay BOOLEAN DEFAULT FALSE
);
--Последовательность для таблицы: in_city_trip
DROP SEQUENCE IF EXISTS trip_client_id_seq;
CREATE SEQUENCE trip_client_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Добавление заказа в городе
DROP FUNCTION IF EXISTS f_new_trip(_client VARCHAR(15),
_places BYTEA,
_in_city BOOLEAN,
_plane BOOLEAN,
_date_plane BIGINT,
_status_trip VARCHAR(15),
_date_start BIGINT,
_price INTEGER,
_babych BOOLEAN,
_smoking BOOLEAN,
_fastup BOOLEAN,
_animal BOOLEAN,
_comment VARCHAR(255),
_pay BOOLEAN,
_srv VARCHAR(15));
CREATE OR REPLACE FUNCTION f_new_trip(_client VARCHAR(15),
_places BYTEA,
_in_city BOOLEAN,
_plane BOOLEAN,
_date_plane BIGINT,
_status_trip VARCHAR(15),
_date_start BIGINT,
_price INTEGER,
_babych BOOLEAN,
_smoking BOOLEAN,
_fastup BOOLEAN,
_animal BOOLEAN,
_comment VARCHAR(255),
_pay BOOLEAN,
_srv VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(252);
_city VARCHAR(15);
BEGIN
_id = 'TR';
SELECT nextval('trip_client_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
SELECT city FROM users WHERE id = _client INTO _city;
INSERT INTO trip_client(id,client,price,places,in_city,city,plane,date_plane,status_trip,date_start,babych,smoking,fastup,animal,comment,pay,srv) 
VALUES(_id,_client,_price,_places,_in_city,_city,_plane,_date_plane,_status_trip,_date_start,_babych,_smoking,_fastup,_animal,_comment,_pay,_srv);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_trip_client('USR7','1'::bytea,FALSE,FALSE,1584857712,'create',1584857712,TRUE,FALSE,FALSE,FALSE,FALSE,'no',FALSE);
SELECT * FROM trip_client;

DROP TABLE IF EXISTS orders;
CREATE TABLE orders (
id VARCHAR(15) NOT NULL PRIMARY KEY,
client VARCHAR(15) REFERENCES users (id) ON DELETE SET NULL,
driver VARCHAR(15),
price INTEGER,
places BYTEA,
in_city BOOLEAN DEFAULT FALSE,
city VARCHAR(15),
srv VARCHAR(15),
duration_waiting INTEGER DEFAULT 180,
plane BOOLEAN DEFAULT FALSE,
date_plane BIGINT,
status_trip VARCHAR(15) NOT NULL,
date_start BIGINT NOT NULL,
date_end BIGINT,
rating_client BOOLEAN DEFAULT TRUE,
--cancel_client VARCHAR(15),
rating_driver BOOLEAN DEFAULT TRUE,
--cancel_driver VARCHAR(15),
cancel VARCHAR(15),
babych BOOLEAN DEFAULT FALSE,
smoking BOOLEAN DEFAULT FALSE,
fastup BOOLEAN DEFAULT FALSE,
animal BOOLEAN DEFAULT FALSE,
comment_exist BOOLEAN DEFAULT FALSE,
comment VARCHAR(255),
pay BOOLEAN DEFAULT FALSE
);

DROP FUNCTION IF EXISTS f_move_order();
CREATE OR REPLACE FUNCTION f_move_order() RETURNS VOID AS $$
DECLARE
BEGIN
WITH moved_trip AS (DELETE FROM trip_client WHERE trip_client.status_trip = 'end' RETURNING *) INSERT INTO orders SELECT * FROM moved_trip;
END;
$$ LANGUAGE plpgsql;

--Таблица dialogs
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"trip" - поездка
--"sender" - отправитель сообщения
--"message" - сообщение
--"date_cont" - время отправления сообщения
DROP TABLE IF EXISTS dialogs;
CREATE TABLE  dialogs (
id VARCHAR(15) NOT NULL PRIMARY KEY,
trip VARCHAR(15) NOT NULL,
sender VARCHAR(15) REFERENCES users (id) ON DELETE SET NULL,
content TEXT NOT NULL,
date_cont BIGINT NOT NULL
);
--Последовательность для таблицы: dialogs
DROP SEQUENCE IF EXISTS dialogs_id_seq;
CREATE SEQUENCE dialogs_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Добавление нового диалога
DROP FUNCTION IF EXISTS f_new_dialogs(_trip VARCHAR(15), _sender VARCHAR(15), _content TEXT, _date_cont BIGINT);
CREATE OR REPLACE FUNCTION f_new_dialogs(_trip VARCHAR(15), _sender VARCHAR(15), _content TEXT, _date_cont BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(252);
BEGIN
_id = 'DL';
SELECT nextval('dialogs_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO dialogs(id,trip,sender,content,date_cont) VALUES(_id,_trip,_sender,_content,_date_cont);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_dialogs('TR1','USR1','Добырй день, забыл указать, что я буду со стороны въезда на стоянку.',1572211281);