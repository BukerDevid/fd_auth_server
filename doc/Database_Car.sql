--Таблица color_auto содержит в себе цвета автомобилей.
--Добавление через функцию f_add_coor_auto().
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"color" - цвет автомобиля. (Уникальное)
--"code" - код цвета в 16-ричном формате. (Уникольно)
DROP TABLE IF EXISTS color_auto;
CREATE TABLE color_auto (
id VARCHAR(15) NOT NULL PRIMARY KEY,
color VARCHAR(30) NOT NULL,
code VARCHAR(7) NOT NULL,
UNIQUE(color),
UNIQUE(code)
);
--Последовательность для таблицы: color_auto
DROP SEQUENCE IF EXISTS color_auto_id_seq;
CREATE SEQUENCE color_auto_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Регистрация нового цвета автомобиля в системе
DROP FUNCTION IF EXISTS f_new_color_auto(_color VARCHAR(30), _code VARCHAR(7));
CREATE OR REPLACE FUNCTION f_new_color_auto(_color VARCHAR(30), _code VARCHAR(7)) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(252);
BEGIN
_id = 'CLRAT';
SELECT nextval('color_auto_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO color_auto(id,color,code) VALUES(_id,_color,_code);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_color_auto('Красный','#FF0000');
SELECT f_new_color_auto('Зеленный','#00FF00');
SELECT f_new_color_auto('Синий','#0000FF');
SELECT * FROM color_auto;

--Таблица mark содержит в себе марки автомобилей.
--Добавляется через функцию f_add_mark().
--"id" - уникальный индетификатор (Уникально, внешний ключ)
--"mark" - наименование марки. (Уникально)
DROP TABLE IF EXISTS mark;
CREATE TABLE mark (
id VARCHAR(15) NOT NULL PRIMARY KEY,
mark VARCHAR(20) NOT NULL,
UNIQUE(mark)
);
--Последовательность для таблицы: mark
DROP SEQUENCE IF EXISTS mark_id_seq;
CREATE SEQUENCE mark_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Регистрация новой марки в системе
DROP FUNCTION IF EXISTS f_new_mark(_mark VARCHAR(15));
CREATE OR REPLACE FUNCTION f_new_mark(_mark VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(252);
BEGIN
_id = 'MRK';
SELECT nextval('mark_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO mark(id,mark) VALUES(_id,_mark);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_mark('Toyota');
SELECT f_new_mark('Lada (ВАЗ)');
SELECT * FROM mark;

--Таблица cars содержит в себе информацию о автомобиле.
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"mark" - марка автомобиля. (Связь с mark)
--"color" - цвет автомобиля. (Связь с color_auto)
--"num_ger" - регистрационный номер. (Уникальное)
--"num_documents" - номер документа. (Уникальное)
DROP TABLE IF EXISTS cars;
CREATE TABLE cars (
id VARCHAR(15) NOT NULL PRIMARY KEY,
mark VARCHAR(15) REFERENCES mark (id) ON UPDATE CASCADE,
color VARCHAR(15) REFERENCES color_auto (id) ON UPDATE CASCADE,
num_reg VARCHAR(15) NOT NULL,
owner_user VARCHAR(15) DEFAULT '',
owner_cabinet VARCHAR(15) DEFAULT '',
rent BOOLEAN DEFAULT FALSE,
rent_user VARCHAR(15) DEFAULT '',
rent_id VARCHAR(15),
block_auto BOOLEAN DEFAULT FALSE,
verification BOOLEAN DEFAULT FALSE,
UNIQUE(id),
UNIQUE(num_reg),
UNIQUE(pts_documents)
);

--Последовательность для таблицы: cars
DROP SEQUENCE IF EXISTS cars_id_seq;
CREATE SEQUENCE cars_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Регистрация нового автомобиля
DROP FUNCTION IF EXISTS f_new_car(_mark VARCHAR(15), _color VARCHAR(15), _num_reg VARCHAR(15));
CREATE OR REPLACE FUNCTION f_new_car(_mark VARCHAR(15), _color VARCHAR(15), _num_reg VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(252);
BEGIN
_id = 'CR';
SELECT nextval('cars_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO cars(id, mark, color, num_reg) VALUES(_id, _mark, _color, _num_reg);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Привязка автомобиля пользователю
DROP FUNCTION IF EXISTS f_keep_owner_user(_id VARCHAR(15), _owner_user VARCHAR(15));
CREATE OR REPLACE FUNCTION f_keep_owner_user(_id VARCHAR(15), _owner_user VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
_owner VARCHAR(15);
BEGIN
SELECT owner_cabinet FROM cars WHERE id = _id INTO _owner;
IF _owner != '' AND _owner != NULL THEN
RETURN '';
END IF;
SELECT owner_user FROM cars WHERE id = _id INTO _owner;
IF _owner != '' AND _owner != NULL THEN
RETURN '';
END IF;
UPDATE cars SET owner_user = _owner_user WHERE id = _id;
UPDATE drivers SET car = _id WHERE id = _owner_user; 
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Отвязка автомобиля от всех владельцев
DROP FUNCTION IF EXISTS f_del_owner(_id VARCHAR(15));
CREATE OR REPLACE FUNCTION f_del_owner(_id VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
_owner VARCHAR(15);
BEGIN
UPDATE cars SET owner_user = '' WHERE id = _id;
SELECT owner_user FROM cars WHERE id = _id INTO _owner;
IF _owner = '' THEN
UPDATE drivers SET car = '' WHERE car = _id; 
RETURN _id;
END IF;
SELECT owner_cabinet FROM cars WHERE id = _id INTO _owner;
IF _owner = '' THEN
UPDATE cars SET owner_cabinet = '' WHERE id = _id;
PERFORM f_del_cars_cabinet(_id);
RETURN _id;
END IF;
RETURN '';
END;
$$ LANGUAGE plpgsql;
--Привязка автомобиля к кабинету
DROP FUNCTION IF EXISTS f_keep_owner_cab(_id VARCHAR(15), _owner_cabinet VARCHAR(15), _date_keep BIGINT);
CREATE OR REPLACE FUNCTION f_keep_owner_cab(_id VARCHAR(15), _owner_cabinet VARCHAR(15), _date_keep BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
_owner VARCHAR(15);
BEGIN
SELECT owner_cabinet FROM cars WHERE id = _id INTO _owner;
IF _owner != '' THEN
RETURN '';
END IF;
SELECT owner_user FROM cars WHERE id = _id INTO _owner;
IF _owner != '' THEN
RETURN '';
END IF;
UPDATE cars SET owner_cabinet = _owner_cabinet WHERE id = _id;
PERFORM f_new_cars_cabinet(_owner_cabinet,_id,_date_keep);
RETURN _id;
END;
$$ LANGUAGE plpgsql;

--Арендовать автомобиль
DROP FUNCTION IF EXISTS f_start_rent_car(_car VARCHAR(15), _cabinet VARCHAR(15), _tenant VARCHAR(15), _date_keep BIGINT);
CREATE OR REPLACE FUNCTION f_start_rent_car(_car VARCHAR(15), _cabinet VARCHAR(15), _tenant VARCHAR(15), _date_keep BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
BEGIN
IF (SELECT given_user FROM cars_cabinet WHERE car = _car) != _tenant AND
(SELECT keep_user FROM cars_cabinet WHERE car = _car) != FALSE AND 
(SELECT del_cars FROM cars_cabinet WHERE car = _car) != FALSE AND
(SELECT owner_cabinet FROM cars WHERE id = _car) != _cabinet THEN
RETURN '';
END IF;
UPDATE cars SET rent_user = _tenant, rent_id = (SELECT f_start_history_rent(_cabinet,_car,_tenant,_date_keep)) WHERE id = _car;
UPDATE cars_cabinet SET keep_user = TRUE WHERE car = _car;
UPDATE drivers SET car_arend = _car WHERE id = _tenant;
RETURN _car;
END;
$$ LANGUAGE plpgsql;

--Принять автомобиль
DROP FUNCTION IF EXISTS f_end_rent_car(_hr VARCHAR(15), _date_leave BIGINT);
CREATE OR REPLACE FUNCTION f_end_rent_car(_hr VARCHAR(15), _date_leave BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
_check_rent VARCHAR(15);
BEGIN
SELECT id FROM cars WHERE rent_id = _hr INTO _check_rent;
IF _check_rent = '' THEN
RETURN '';
END IF;
PERFORM f_end_history_rent(_hr, _date_leave);
UPDATE cars_cabinet SET keep_user = FALSE WHERE car = _check_rent;
UPDATE cars SET rent_user = '', rent_id = '' WHERE rent_id = _hr;
RETURN _hr;
END;
$$ LANGUAGE plpgsql;
--Обновить данные автомобиля
DROP FUNCTION IF EXISTS f_update_car(_id VARCHAR(15), _color VARCHAR(15), _mark VARCHAR(15));
CREATE OR REPLACE FUNCTION f_update_car(_id VARCHAR(15), _color VARCHAR(15), _mark VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
_tmp_color VARCHAR(15);
_tmp_mark VARCHAR(15);
BEGIN
IF _color != '' THEN
SELECT id FROM color_auto WHERE id = _color INTO _tmp_color;
IF _tmp_color = _color THEN
UPDATE cars SET color = _color WHERE id = _id;
END IF;
END IF;
IF _mark != '' THEN
SELECT id FROM mark WHERE id = _mark INTO _tmp_mark;
IF _tmp_mark = _mark THEN
UPDATE cars SET mark = _mark WHERE id = _id;
END IF;
END IF;
RETURN _id;
END
$$ LANGUAGE plpgsql;

--Default:
SELECT f_new_car('MRK1','CLRAT2','A001AA111RUS','00АА','000000');
SELECT f_new_car('MRK2','CLRAT3','A002AA111RUS','00АА','000001');
SELECT f_new_car('MRK1','CLRAT2','A003AA111RUS','00АА','000002');
SELECT f_new_car('MRK2','CLRAT1','A004AA111RUS','00АА','000003');

--Таблица history_rent содержит информацию о истории аренды автомобилей
--водителями у партнеров
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"cabinet" - кабинет, который предоставлял автомобиль
--"car" - автомобиль, который был арендован
--"tenant" - пользователь, который арендует автомобиль (арендатор)
--"start_rent" - начало аренды
--"end_rent" - окончание аренды
DROP TABLE IF EXISTS history_rent;
CREATE TABLE history_rent (
id VARCHAR(15) NOT NULL PRIMARY KEY,
cabinet VARCHAR(15) REFERENCES cabinet_ps (id) ON DELETE SET NULL,
car VARCHAR(15) REFERENCES cars (id) ON DELETE SET NULL,
tenant VARCHAR(15) REFERENCES users (id) ON DELETE SET NULL,
start_rent BIGINT NOT NULL, 
end_rent BIGINT
);
--Последовательность для таблицы: history_rent
DROP SEQUENCE IF EXISTS history_rent_id_seq;
CREATE SEQUENCE history_rent_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Зафиксировать аренду автомобиля
DROP FUNCTION IF EXISTS f_start_history_rent(_cabinet VARCHAR(15), _car VARCHAR(15), _tenant VARCHAR(15), _start_rent BIGINT);
CREATE OR REPLACE FUNCTION f_start_history_rent(_cabinet VARCHAR(15), _car VARCHAR(15), _tenant VARCHAR(15), _start_rent BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(13);
BEGIN
_id = 'HR';
SELECT nextval('history_rent_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO history_rent(id, cabinet, car, tenant, start_rent) VALUES(_id, _cabinet, _car, _tenant, _start_rent);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_start_history_rent('CBPS1','CR4','USR1',1572115340);
--Законфить фиксацию аренды автомобиля
DROP FUNCTION IF EXISTS f_end_history_rent(_id VARCHAR(15), _time_end BIGINT);
CREATE OR REPLACE FUNCTION f_end_history_rent(_id VARCHAR(15), _time_end BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
_id_tenant VARCHAR(15);
BEGIN
UPDATE drivers SET car_arend = '' WHERE id = (SELECT tenant FROM history_rent WHERE id = _id);
UPDATE history_rent SET end_rent = _time_end WHERE id = _id;
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_end_history_rent('HR1',1572116040);

-- --Таблица freight
-- --"id" - уникальный индетификатор. (Уникальное, внешний ключ)
-- --"driver" - водитель
-- --"price" - цена за час
-- --"phone" - телефон
-- --"max_mass" - максимальный вес
-- DROP TABLE IF EXISTS freight;
-- CREATE TABLE freight (
-- id VARCHAR(15) NOT NULL PRIMARY KEY,
-- driver VARCHAR(15) REFERENCES users (id) ON DELETE SET NULL,
-- price NUMERIC NOT NULL CHECK (price > 0),
-- phone VARCHAR(10) NOT NULL,
-- about VARCHAR(256),
-- max_mass INTEGER NOT NULL,
-- del_freight BOOLEAN DEFAULT FALSE,
-- UNIQUE(driver)
-- );
-- --Последовательность для таблицы: freight
-- DROP SEQUENCE IF EXISTS freight_id_seq;
-- CREATE SEQUENCE freight_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
-- --Добавление объявление грузоперевозки.
-- DROP FUNCTION IF EXISTS f_new_freight(_driver VARCHAR(15), _price NUMERIC, _phone VARCHAR(10), _about VARCHAR(256), _max_mass INTEGER);
-- CREATE OR REPLACE FUNCTION f_new_freight(_driver VARCHAR(15), _price NUMERIC, _phone VARCHAR(10), _about VARCHAR(256), _max_mass INTEGER) RETURNS VARCHAR(15) AS $$
-- DECLARE
-- _id VARCHAR(15);
-- _num_nextval VARCHAR(13);
-- BEGIN
-- IF (SELECT driver FROM freight WHERE driver = _driver) != '' THEN
-- UPDATE freight SET price = _price, phone = _phone, about = _about, max_mass = _max_mass, del_freight = FALSE WHERE driver = _driver;
-- return 'update';
-- END IF;
-- _id = 'FR';
-- SELECT nextval('freight_id_seq') INTO _num_nextval;
-- _id = CONCAT(_id,_num_nextval);
-- INSERT INTO freight(id, driver, price, phone, about, max_mass) VALUES(_id, _driver, _price, _phone, _about, _max_mass);
-- RETURN _id;
-- END;
-- $$ LANGUAGE plpgsql;
-- --Удаление объявление грузоперевозки.
-- DROP FUNCTION IF EXISTS f_del_freight(_driver VARCHAR(15));
-- CREATE OR REPLACE FUNCTION f_del_freight(_driver VARCHAR(15)) RETURNS VARCHAR(15) AS $$
-- DECLARE
-- BEGIN
-- UPDATE freight SET del_freight = TRUE WHERE driver = _driver;
-- RETURN _driver;
-- END;
-- $$ LANGUAGE plpgsql;
-- --Default:
-- SELECT f_new_freight('USR1',400,'9825471689','',550);