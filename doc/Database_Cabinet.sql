
--Таблица cabinet_ps содержит инфомрацию о кажинетах партнеров по сотрудничеству
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"cod" - код доступа
--"pwd" - результат хэш функции (пароля кабинета)
--"title" - название кабинета
--"num_cart" - токен карты
--"city" - город
--"del_cabinet" - удален кабинет
--"del_commit" - причина удаления кабинета
--"limit_session" - ограниченные сессии ( Да - true, нет - false)
--"limit_out_session" - количество выданных сессий = (cars_cabines*2)+(cars_cabines-2)
--"date_recovery" - дата восстановления limit_out_session в состояние по умолчанию
DROP TABLE IF EXISTS cabinet_ps;
CREATE TABLE cabinet_ps (
id VARCHAR(15) NOT NULL PRIMARY KEY,
title_ps VARCHAR(80) NOT NULL,
title_scar VARCHAR(80) NOT NULL,
city VARCHAR(15) REFERENCES cities (id) ON DELETE SET NULL,
del_cabinet BOOLEAN DEFAULT FALSE,
del_commit VARCHAR(256),
limit_session BOOLEAN DEFAULT TRUE,
limit_session_out INTEGER DEFAULT 3,
date_recovery BIGINT DEFAULT 0,
lat REAL NOT NULL,
lon REAL NOT NULL,
addr TEXT NOT NULL
);
--Последовательность для таблицы: cabinet_ps
DROP SEQUENCE IF EXISTS cabinet_ps_id_seq;
CREATE SEQUENCE cabinet_ps_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Регистрация нового кабинета
DROP FUNCTION IF EXISTS f_new_cabinet_ps(_title_ps VARCHAR(80), _title_scar VARCHAR(80), _city VARCHAR(15), _lat REAL, _lon REAL, _addr TEXT);
CREATE OR REPLACE FUNCTION f_new_cabinet_ps(_title_ps VARCHAR(80), _title_scar VARCHAR(80), _city VARCHAR(15), _lat REAL, _lon REAL, _addr TEXT) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_iter VARCHAR(15);
_num_nextval VARCHAR(252);
_agent_in_city VARCHAR(15)[]; 
_max_agent INTEGER;
_count_agent_in_city INTEGER;
BEGIN
SELECT unit FROM cities WHERE id = _city INTO _max_agent;
SELECT cabinet_ps FROM cities WHERE id = _city INTO _agent_in_city;
SELECT cardinality(cabinet_ps) FROM cities WHERE id = _city INTO _count_agent_in_city;
IF _count_agent_in_city >= _max_agent THEN
RETURN 'LIMIT';
END IF;
_id = 'CBPS';
SELECT nextval('cabinet_ps_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO cabinet_ps(id, title_ps, title_scar, city, lat, lon, addr) VALUES(_id, _title_ps, _title_scar, _city, _lat, _lon, _addr);
_agent_in_city = _agent_in_city || _id;
UPDATE cities SET cabinet_ps = _agent_in_city WHERE id = _city;
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Регистрация кабинета FullDriver
DROP FUNCTION IF EXISTS f_new_cabinet_fd(_city VARCHAR(15));
CREATE OR REPLACE FUNCTION f_new_cabinet_fd(_city VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_iter VARCHAR(15);
_num_nextval VARCHAR(252);
_agent_in_city VARCHAR(15)[]; 
_max_agent INTEGER;
_count_agent_in_city INTEGER;
BEGIN
SELECT unit FROM cities WHERE id = _city INTO _max_agent;
SELECT cabinet_ps FROM cities WHERE id = _city INTO _agent_in_city;
SELECT cardinality(cabinet_ps) FROM cities WHERE id = _city INTO _count_agent_in_city;
IF _count_agent_in_city >= _max_agent THEN
RETURN 'LIMIT';
END IF;
_id = 'CBFD';
SELECT nextval('cabinet_ps_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO cabinet_ps(id, title_ps, title_scar, city, lat, lon) VALUES(_id, 'FullDriver', 'FullDriver', _city, 0.0, 0.0);
_agent_in_city = _agent_in_city || _id;
UPDATE cities SET cabinet_ps = _agent_in_city WHERE id = _city;
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Удаление кабинета
DROP FUNCTION IF EXISTS f_del_cabinet_ps(_id VARCHAR(15), _comm VARCHAR(256));
CREATE OR REPLACE FUNCTION f_del_cabinet_ps(_id VARCHAR(15), _comm VARCHAR(256)) RETURNS VARCHAR(15) AS $$
DECLARE
_iter VARCHAR(15);
_without_del_cab VARCHAR(15)[];
_agent_in_city VARCHAR(15)[];
_city VARCHAR(15);
BEGIN
SELECT city FROM cabinet_ps WHERE id = _id INTO _city;
SELECT cabinet_ps FROM cities WHERE id = _city INTO _agent_in_city;
FOREACH _iter IN ARRAY _agent_in_city LOOP
IF _iter != _id THEN
_without_del_cab = _without_del_cab || _iter;
END IF;
END LOOP; 
UPDATE cabinet_ps SET del_cabinet = TRUE, del_commit = _comm WHERE id = _id; 
UPDATE cities SET cabinet_ps = _without_del_cab WHERE id = _city;
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Восстановление кабинета
DROP FUNCTION IF EXISTS f_recovery_cabinet_ps(_id VARCHAR(15));
CREATE OR REPLACE FUNCTION f_recovery_cabinet_ps(_id VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
_iter VARCHAR(15);
_with_del_cab VARCHAR(15)[];
_agent_in_city VARCHAR(15)[];
_city VARCHAR(15);
BEGIN
SELECT city FROM cabinet_ps WHERE id = _id INTO _city;
SELECT cabinet_ps FROM cities WHERE id = _city INTO _agent_in_city;
_with_del_cab = _agent_in_city || _id;
UPDATE cabinet_ps SET del_cabinet = FALSE, del_commit = '' WHERE id = _id;  
UPDATE cities SET cabinet_ps = _with_del_cab WHERE id = _city;
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Обновление данных кабинета
DROP FUNCTION IF EXISTS f_update_cabinet_ps(_id VARCHAR(15), _title_ps VARCHAR(80), _title_scar VARCHAR(80), _city VARCHAR(15), _lat REAL, _lon REAL, _addr TEXT);
CREATE OR REPLACE FUNCTION f_update_cabinet_ps(_id VARCHAR(15), _title_ps VARCHAR(80), _title_scar VARCHAR(80), _city VARCHAR(15), _lat REAL, _lon REAL, _addr TEXT) RETURNS VARCHAR(15) AS $$
DECLARE
BEGIN
IF _title_ps != '' THEN
UPDATE cabinet_ps SET title_ps = _title_ps WHERE id = _id; 
END IF;
IF _title_scar != '' THEN
UPDATE cabinet_ps SET title_scar = _title_scar WHERE id = _id;
END IF;
IF _city != '' THEN 
UPDATE cabinet_ps SET city = _city WHERE id = _id;
END IF;
IF _lat != 0 THEN
UPDATE cabinet_ps SET lat = _lat WHERE id = _id;
END IF;
IF _lon != 0 THEN
UPDATE cabinet_ps SET lon = _lon WHERE id = _id;
END IF;
IF _addr != '' THEN
UPDATE cabinet_ps SET addr = _addr WHERE id = _id;
END IF;
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_cabinet_ps('389abf937a80d6ef3c6df67df2f265a5fc682d536bb23535a3d9decc9ab34baa','Бытрое Такси','Нижневартовск №1','88w792iwfjj7eu7u8009ei48ru48u383ie93u','CT1');
SELECT f_new_cabinet_ps('389abf937a80d6ef3c6df67df2f265a5fc682d536bb23535a3d9decc9ab34baa','Мое Такси','Мегион №1','88w792iwfjj7eu7u8009ei48ru48u383ie93u','CT2');
SELECT f_new_cabinet_ps('389abf937a80d6ef3c6df67df2f265a5fc682d536bb23535a3d9decc9ab34baa','Таксопарк №1','Нижневартовск №2','88w792iwfjj7eu7u8009ei48ru48u383ie93u','CT1');
SELECT id, title_ps, title_scar, city FROM cabinet_ps;

--Таблица cars_cabinet содержит информацию о автомобилей привязанных к 
--кабинету партнеров
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"cabinet" - кабинет, которому принадлежит автомобиль
--"car" - автомобильы
--"rent" - True - свободна/False - занята 
--"date_keep" - дата привязки
--"date_out" - дата отвязки
DROP TABLE IF EXISTS cars_cabinet;
CREATE TABLE cars_cabinet (
id VARCHAR(15) NOT NULL PRIMARY KEY,
cabinet VARCHAR(15) NOT NULL,
car VARCHAR(15) REFERENCES cars (id) ON DELETE SET NULL,
date_keep BIGINT NOT NULL,
given_user VARCHAR(15),
keep_user BOOLEAN,
del_cars BOOLEAN DEFAULT FALSE,
UNIQUE(car)
);
--Последовательность для таблицы: cars_cabinet
DROP SEQUENCE IF EXISTS cars_cabinet_id_seq;
CREATE SEQUENCE cars_cabinet_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Привязка автомобиля к кабинету
DROP FUNCTION IF EXISTS f_new_cars_cabinet(_cabinet VARCHAR(15), _car VARCHAR(15), _date_keep BIGINT);
CREATE OR REPLACE FUNCTION f_new_cars_cabinet(_cabinet VARCHAR(15), _car VARCHAR(15), _date_keep BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(11);
_check_car VARCHAR(15);
BEGIN
SELECT id FROM cars_cabinet WHERE car = _car INTO _id;
IF _id != '' THEN
UPDATE cars_cabinet SET del_cars = FALSE WHERE car = _car;
RETURN _id;
END IF;
SELECT nextval('cars_cabinet_id_seq') INTO _num_nextval;
_id = 'CRCB';
_id = CONCAT(_id,_num_nextval);
INSERT INTO cars_cabinet(id, cabinet, car, date_keep) VALUES(_id, _cabinet, _car, _date_keep);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Отвязка автомобиля от кабинета
DROP FUNCTION IF EXISTS f_del_cars_cabinet(_car VARCHAR(15));
CREATE OR REPLACE FUNCTION f_del_cars_cabinet(_car VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
BEGIN
UPDATE cars_cabinet SET del_cars = TRUE WHERE car = _car;
RETURN _car;
END;
$$ LANGUAGE plpgsql;
--Дать в аренду
DROP FUNCTION IF EXISTS f_given_out_car_cabinet(_car VARCHAR(15), _tenant VARCHAR(15));
CREATE OR REPLACE FUNCTION f_given_out_car_cabinet(_car VARCHAR(15), _tenant VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
BEGIN
PERFORM f_taken_car_cabinet(_car);
UPDATE cars_cabinet SET given_user = _tenant WHERE car = _car;
RETURN _car;
END;
$$ LANGUAGE plpgsql;
--Снять с аренды
DROP FUNCTION IF EXISTS f_taken_car_cabinet(_car VARCHAR(15));
CREATE OR REPLACE FUNCTION f_taken_car_cabinet(_car VARCHAR(15)) RETURNS VARCHAR(15) AS $$
DECLARE
BEGIN
UPDATE cars_cabinet SET given_user = '' WHERE car = _car;
RETURN _car;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_cars_cabinet('CBPS1','CR1',1572115330);
SELECT f_new_cars_cabinet('CBPS3','CR2',1572115340);
SELECT * FROM cars_cabinet;
