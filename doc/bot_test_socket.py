import threading
import websocket
import ssl
import json
try:
    import thread
except ImportError:
    import _thread as thread
import time

def on_message(ws, message):
    print("Read - ", message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    print("Up connect")
    def run(*args):
        for i in range(1):
            # time.sleep(200)
            data = {
                "type": "create",
                "data": {
                    "places": [
                        {
                            "city": "ХМАО-Югра Нижневартовск",
                            "addr": "60 лет Октября, 47",
                            "type_porch": 1,
                            "porch": 1,
                            "Lat": 111222.878,
                            "Lon": 123422.7878
                        },
                        {
                            "city": "ХМАО-Югра Нижневартовск",
                            "addr": "Ленина, 7",
                            "type_porch": 0,
                            "porch": 0,
                            "Lat": 1112223332,
                            "Lon": 1234224435
                        }
                    ],
                    "options": {
                        "price": 120,
                        "babych": True,
                        "somking": False,
                        "fastup": False,
                        "comment": "Я буду напротив магазина",
                        "animal": False,
                        "plan_order": False,
                        "time_plan": "",
                        "pay": False
                    }
                }
            }

            data = json.dumps(data)
            ws.send(data)
            print('Iteration - ' + str(i) + ', I sleep on 5....')
        while(True):
            com = input("Enter 'q' for exit >> ")
            if com == 'q':
                ws.close()
                break
            print("thread terminating...")
    thread.start_new_thread(run, ())

def new_con():
    #print(str(i) + ' - start!')
    websocket.enableTrace(True)
    ws = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
    ws = websocket.WebSocketApp("wss://scar.metro86.ru:4740/fd/order?token=130a19ed330c76a7f74485a5bbca7db3daf8154e93e41a8aa3a80fcf80eeb5e3_eded6aabb5c0baf143b064f6c467289fe37fa1792c4ebfa47b08f1259a9595c3_981d9e161fbe5305fd3262c588497f0a",
                            on_message = on_message,
                            on_error = on_error,
                            on_close = on_close)
    try:
        ws.on_open = on_open
    except:
        print("error UpSocket")
    ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})
    try:
        ws.on_open(ws)
    except:
        print("error UpSocket")

for i in range(1):
    time.sleep(0.1)
    print("start "+str(i))
    eth1 = threading.Thread(target=new_con, args=())
    eth1.start()