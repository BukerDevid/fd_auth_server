import threading
import websocket
import ssl
import json
try:
    import thread
except ImportError:
    import _thread as thread
import time

def on_message(ws, message):
    print("Read - ", message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    print("Up connect")
    def run(*args):
        data = {
            "type": "create",
            "data": {
                "places": [
                    {
                        "city": "Нижневартовск",
                        "addr": "60 лет Октября, 47",
                        "type_porch": 1,
                        "porch": 3,
                        "Lat": 1.878,
                        "Lon": 1.7878
                    },
                    {
                        "city": "Нижневартовск",
                        "addr": "Ленина, 7",
                        "type_porch": 1,
                        "porch": 1,
                        "Lat": 1.112223332,
                        "Lon": 1.234224435
                    }
                ],
                "options": {
                    "price": 120,
                    "babych": True,
                    "smoking": True,
                    "fastup": False,
                    "comment": "Я буду напротив магазина",
                    "animal": False,
                    "plan_order": False,
                    "time_plan": "",
                    "pay": False
                }
            }
        }
        data = json.dumps(data)
        ws.send(data)
        time.sleep(5)
        ws.send(data)
        # print('Data create order send -> WS')
        # time.sleep(10)
        # data = {
        #     "type": "replay",
        #     "data": 140
        # }
        # data = json.dumps(data)
        # ws.send(data)
        print('Data replay send -> WS')
        time.sleep(5)
        # data = {
        #     "type": "start",
        #     "data": {
        #         "order": "",
        #         "driver": "USR12",
        #         "duration": 3
        #     }
        # }
        # data = json.dumps(data)
        # ws.send(data)
        print('Data start order send -> WS')
        time.sleep(30)
        data = {
            "type": "cancel",
            "data": ""
        }
        ata = json.dumps(data)
        ws.send(ata)
        while(True):
            com = input("Enter 'q' for exit >> ")
            if com == 'q':
                ws.close()
                break
            print("thread terminating...")
    thread.start_new_thread(run, ())

def new_con():
    print(str(i) + ' - start!')
    websocket.enableTrace(True)
    ws = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
    ws = websocket.WebSocketApp("wss://scar.metro86.ru:4740/fd/order?token=37679b2d5a9c2d317de7df8f5c609130ab747be7afc293fd127db0f5c5465b58_4e47bea7ef2005825e6473db7d2fdbf21001870c135e535f8debabf0441570de_0f5db19d5995064469a4af9b43d64c33",
                            on_message = on_message,
                            on_error = on_error,
                            on_close = on_close)
    try:
        ws.on_open = on_open
    except:
        print("error UpSocket")
    ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})
    try:
        ws.on_open(ws)
    except:
        print("error UpSocket")

for i in range(1):
    time.sleep(0.1)
    print("start "+str(i))
    eth1 = threading.Thread(target=new_con, args=())
    eth1.start()