
--Таблиа regions содержит информацию о регионе
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"region" - Наименование региона
DROP TABLE IF EXISTS regions;
CREATE TABLE regions (
id VARCHAR(15) NOT NULL PRIMARY KEY,
region VARCHAR(15) NOT NULL,
utc INTEGER NOT NULL
);
--Последовательность для таблицы: regions
DROP SEQUENCE IF EXISTS regions_id_seq;
CREATE SEQUENCE regions_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Function add regions
DROP FUNCTION IF EXISTS f_new_regions(_region VARCHAR(15), _utc INTEGER);
CREATE OR REPLACE FUNCTION f_new_regions(_region VARCHAR(15), _utc INTEGER) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(13);
BEGIN
_id = 'REG';
SELECT nextval('regions_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO regions(id, region, utc) VALUES(_id, _region, _utc);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_regions('ХМАО-Югра',5);
SELECT f_new_regions('Тюменская обл.',5);
SELECT * FROM regions;

--Таблица cities содержит информацию о городах.
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"unit" - количество партнеров по сотрудничеству в городе
--"region" - регион
--"active" - функцианирует ли город да(true)/нет(false)
--"city" - наименование города, указывается с припиской региона "Нижневартовск". (Уникально)
--"en-city" - наименование города на английском языке
--"city_lat" - координата долгота
--"city_lon" - координата широта
--"min_price"
--"time_live_order_created" - время жизни заказа в статусе - "created", по умолчанию - 2 мин.
--"time_waiting_client" - время жизни заказа в статусе - "driver_ready", по умолчанию - 5 мин.
--"time_live_order_trip" - время жизни заказа в статусе - "begin_trip", по умолчанию - 1 час 30 мин.
--"time_waiting_check" - время ожидания отклика от приложения или пользователя, по умолчанию - 2 мин.
--"time_waiting_driver" - время ко времени ожидания водителя, по умолчанию - 2 мин.
DROP TABLE IF EXISTS cities;
CREATE TABLE cities (
id VARCHAR(15) NOT NULL PRIMARY KEY,
unit INTEGER NOT NULL,
region VARCHAR(15) REFERENCES regions (id) ON DELETE CASCADE,
active BOOLEAN NOT NULL,
cabinet_ps VARCHAR(15)[],
city VARCHAR(15) NOT NULL,
en_city VARCHAR(24), 
city_lat REAL, 
city_lon REAL,
min_price INTEGER DEFAULT 0,
time_live_order_created INTEGER DEFAULT 120,
time_waiting_client INTEGER DEFAULT 300,
time_live_order_trip INTEGER DEFAULT 5400,
time_waiting_check INTEGER DEFAULT 120,
time_waiting_driver INTEGER DEFAULT 120,
UNIQUE(region,city)
);
--Последовательность для таблицы: cities
DROP SEQUENCE IF EXISTS cities_id_seq;
CREATE SEQUENCE cities_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Function add cities
DROP FUNCTION IF EXISTS f_new_cities(_unit INTEGER, _region VARCHAR(15), _active BOOLEAN, _city VARCHAR(15), _en_city VARCHAR(24), _city_lat REAL, _city_lon REAL, _min_price INTEGER);
CREATE OR REPLACE FUNCTION f_new_cities(_unit INTEGER, _region VARCHAR(15), _active BOOLEAN, _city VARCHAR(15), _en_city VARCHAR(24), _city_lat REAL, _city_lon REAL, _min_price INTEGER) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(13);
BEGIN
_id = 'CT';
SELECT nextval('cities_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO cities(id, unit, region, active, city, en_city, city_lat, city_lon, min_price) VALUES(_id, _unit, _region, _active, _city, _en_city, _city_lat, _city_lon, _min_price);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_cities(2,'REG1',TRUE,'Нижневартовск','Nizhnevartovsk',60.9393,76.5724,100);
SELECT f_new_cities(1,'REG1',TRUE,'Мегион','Megion',61.0347,76.1067,80);
SELECT f_new_cities(1,'REG1',TRUE,'Излучинск','Isluchinsk',60.9546,76.8935,80);
SELECT f_new_cities(0,'REG1',FALSE,'Сургут','Surgut',61.2553,73.419,120);
SELECT f_new_cities(0,'REG2',FALSE,'Тюмень','Tiumen',57.149,65.5509,120);
SELECT * FROM cities;

--Таблица start_data
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"title" - 
--"value_data" - 
--"img" - 
DROP TABLE IF EXISTS start_data;
CREATE TABLE start_data (
id SERIAL PRIMARY KEY NOT NULL,
title VARCHAR(100),
value_data VARCHAR(150),
img VARCHAR(100)
);
--SQL function for SCAR database

--Таблица map_note
DROP TABLE IF EXISTS map_note;
CREATE TABLE map_note (
    id SERIAL PRIMARY KEY NOt NULL,
    client VARCHAR(15),
    comment TEXT,
    lat REAL,
    lon REAL,
    complated BOOLEAN DEFAULT FALSE
);

--Таблица dys1984
DROP TABLE IF EXISTS dys1984;
CREATE TABLE dys1984(
id VARCHAR(15) NOT NULL PRIMARY KEY,
pwd VARCHAR(64) NOT NULL,
username VARCHAR(30) NOT NULL,
privileges INTEGER
);

--Последовательность для таблицы: dys1984
DROP SEQUENCE IF EXISTS dys1984_id_seq;
CREATE SEQUENCE dys1984_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Function add new admin: Регистрация нового администратора в системе
DROP FUNCTION IF EXISTS f_new_admin(_username VARCHAR(30),_pass VARCHAR(64),_priv INTEGER);
CREATE OR REPLACE FUNCTION f_new_admin(_username VARCHAR(30),_pass VARCHAR(64),_priv INTEGER) RETURNS VARCHAR(15) AS $$
DECLARE
_id_user VARCHAR(15);
_num_nextval VARCHAR(13);
BEGIN
_id_user = 'BB';
SELECT nextval('dys1984_id_seq') INTO _num_nextval;
_id_user = CONCAT(_id_user,_num_nextval);
INSERT INTO dys1984(id,pwd,username,privileges) VALUES(_id_user,_pass,_username,_priv);
RETURN _id_user;
END;
$$ LANGUAGE plpgsql;

DROP TABLE IF EXISTS atokens;
CREATE TABLE atokens (
id VARCHAR(15) REFERENCES dys1984 (id) ON DELETE CASCADE,
token VARCHAR(200) NOT NULL,
date_of_token BIGINT NOT NULL CHECK (date_of_token > 0),
fb_token VARCHAR(200),
data_of_fb_token BIGINT NOT NULL CHECK (data_of_fb_token > 0)
);
