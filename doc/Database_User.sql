--***********************************************************--
--Файл содержит sql код таблиц и функций для формирования
--базы данных для FullDriver, подробное описание можно найти
--в документации проекта. 
--Автор GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru
--***********************************************************--

--Таблица users содержит информацию о пользователях системы.
--Пользователя приложения являются как клиенты, так и водителя.
--По умолчанию указаны данные пользователя как клиента
--Данные о пользователе как о водителе были переопределены в таблице
--Driver.
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"phone" - телефон пользователя
--"pwd" - результат хэш функции (пароля пользователя)
--"username" - пользовательское имя
--"surname" - фамилия водителя
--"middlename" - очество водителя
--"city" - город проживания пользователя
--"rating" - рэйтинг пользователя
--"date_registration" - дата регистрации
--"imag" - ссылка на изображение
--"years" - колличество лет
--"sex" - пол TRUE - Мужской / FALSE - Женский
DROP TABLE IF EXISTS users;
CREATE TABLE users (
id VARCHAR(15) NOT NULL PRIMARY KEY,
phone VARCHAR(10) NOT NULL,
pwd VARCHAR(64) NOT NULL,
username VARCHAR(30) NOT NULL,
surname VARCHAR(30),
middlename VARCHAR(30),
city VARCHAR(15) REFERENCES cities (id) ON DELETE CASCADE,
rating INTEGER DEFAULT 0,
date_reg BIGINT NOT NULL,
img BOOLEAN DEFAULT FALSE,
date_bir VARCHAR(10),
sex BOOLEAN, -- TRUE - man / FALSE - woman
del_user BOOLEAN DEFAULT FALSE, -- TRUE - delete/ FALSE - no deletes
del_date BIGINT DEFAULT 0, --See column if del_user
block_user BOOLEAN DEFAULT FALSE,
block_date BIGINT DEFAULT 0,
image_status BOOLEAN DEFAULT FALSE,
driver BOOLEAN DEFAULT FALSE, -- Status client in last session (this parameter cannot use for check switch mode, but can use for check real time status)
UNIQUE(id),
UNIQUE(phone)
);
--Последовательность для таблицы: users
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Function add new user: Регистрация нового пользователя в системе
DROP FUNCTION IF EXISTS f_new_user(_name VARCHAR(30),_phone VARCHAR(10),_pass VARCHAR(64),_city VARCHAR(15), _date_reg BIGINT, _date_bir VARCHAR(10), _sex BOOLEAN, _image_status BOOLEAN);
CREATE OR REPLACE FUNCTION f_new_user(_name VARCHAR(30),_phone VARCHAR(10),_pass VARCHAR(64),_city VARCHAR(15), _date_reg BIGINT, _date_bir VARCHAR(10), _sex BOOLEAN, _image_status BOOLEAN) RETURNS VARCHAR(15) AS $$
DECLARE
_id_user VARCHAR(15);
_num_nextval VARCHAR(12);
BEGIN
_id_user = 'USR';
SELECT nextval('users_id_seq') INTO _num_nextval;
_id_user = CONCAT(_id_user,_num_nextval);
INSERT INTO users(id,phone,pwd,username,city,date_reg,date_bir,sex,image_status) VALUES(_id_user,_phone,_pass,_name,_city,_date_reg,_date_bir,_sex,_image_status);
RETURN _id_user;
END;
$$ LANGUAGE plpgsql;
--Function delete user
DROP FUNCTION IF EXISTS f_del_user(_id VARCHAR(15), _time BIGINT);
CREATE OR REPLACE FUNCTION f_del_user(_id VARCHAR(15), _time BIGINT) RETURNS BOOLEAN AS $$
DECLARE
BEGIN
UPDATE users SET del_user = TRUE, del_date = _time WHERE id = _id;
RETURN TRUE;
END;
$$ LANGUAGE plpgsql;
--Function recovery deleted
DROP FUNCTION IF EXISTS f_rec_del_user(_id VARCHAR(15), _time BIGINT);
CREATE OR REPLACE FUNCTION f_rec_user(_id VARCHAR(15), _time BIGINT) RETURNS BOOLEAN AS $$
DECLARE
BEGIN
UPDATE users SET del_user = FALSE, del_date = _time WHERE id = _id;
RETURN TRUE;
END;
$$ LANGUAGE plpgsql;
--Function block user
DROP FUNCTION IF EXISTS f_block_user(_id VARCHAR(15), _time BIGINT);
CREATE OR REPLACE FUNCTION f_del_user(_id VARCHAR(15), _time BIGINT) RETURNS BOOLEAN AS $$
DECLARE
BEGIN
UPDATE users SET block_user = TRUE, block_date = _time WHERE id = _id;
RETURN TRUE;
END;
$$ LANGUAGE plpgsql;
--Function recovery block user
DROP FUNCTION IF EXISTS f_rec_block_user(_id VARCHAR(15), _time BIGINT);
CREATE OR REPLACE FUNCTION f_rec_user(_id VARCHAR(15), _time BIGINT) RETURNS BOOLEAN AS $$
DECLARE
BEGIN
UPDATE users SET block_user = FALSE, block_date = _time WHERE id = _id;
RETURN TRUE;
END;
$$ LANGUAGE plpgsql;
--Function update user protected
DROP FUNCTION IF EXISTS f_update_user_secure(_id VARCHAR(15), _phone VARCHAR(10),_pwd VARCHAR(64),_username VARCHAR(30), _surname VARCHAR(30), _middlename VARCHAR(30), _city VARCHAR(15), _date_bir VARCHAR(10), _sex VARCHAR(10));
CREATE OR REPLACE FUNCTION f_update_user_secure(_id VARCHAR(15), _phone VARCHAR(10),_pwd VARCHAR(64),_username VARCHAR(30), _surname VARCHAR(30), _middlename VARCHAR(30), _city VARCHAR(15), _date_bir VARCHAR(10), _sex VARCHAR(10)) RETURNS VARCHAR(15) AS $$
DECLARE
_tmp_phone VARCHAR(10);
_tmp_city VARCHAR(15);
BEGIN
IF _phone != '' THEN
UPDATE users SET phone = _phone WHERE id = _id;
END IF;
IF _pwd != '' THEN
UPDATE users SET pwd = _pwd WHERE id = _id;
END IF;
IF _sex = 'man' THEN
UPDATE users SET sex = TRUE WHERE id = _id;
END IF;
IF _sex = 'woman' THEN
UPDATE users SET sex = FALSE WHERE id = _id;
END IF;
IF _username != '' THEN
UPDATE users SET username = _username WHERE id = _id;
END IF;
IF _surname != '' THEN
UPDATE users SET surname = _surname WHERE id = _id;
END IF;
IF _middlename != '' THEN
UPDATE users SET middlename = _middlename WHERE id = _id;
END IF;
IF _city != '' THEN
SELECT id FROM cities WHERE id = _city INTO _tmp_city;
IF _tmp_city != '' THEN
UPDATE users SET city = _city WHERE id = _id;
END IF;
END IF;
IF _date_bir != '' THEN
UPDATE users SET date_bir = _date_bir WHERE id = _id;
END IF;
RETURN _id;
END
$$ LANGUAGE plpgsql;
--Switch mode user. With driver on client and with client on driver
DROP FUNCTION IF EXISTS f_switch_user_mode(_id VARCHAR(15));
CREATE OR REPLACE FUNCTION f_switch_user_mode(_id VARCHAR(15)) RETURNS BOOLEAN AS $$
DECLARE
_tmp_status BOOLEAN;
BEGIN
SELECT driver FROM users WHERE id = _id INTO _tmp_status;
IF _tmp_status = TRUE THEN
UPDATE users SET driver = FALSE WHERE id = _id;
RETURN FALSE;
END IF;
IF _tmp_status = FALSE THEN
UPDATE users SET driver = TRUE WHERE id = _id;
RETURN TRUE;
END IF;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT * FROM f_new_user('Василий','9825471689','bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a','CT1',1571943777,'06/06/1996',TRUE,FALSE);
SELECT * FROM f_new_user('Андрей','9138622971','bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a','CT1',1571943788,'01/06/1996',TRUE,FALSE);
SELECT * FROM f_new_user('Нухкади','9963298640','bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a','CT1',1571943799,'07/06/1996',TRUE,FALSE);
SELECT * FROM f_new_user('Иван','9998887766','bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a','CT1',1571943811,'08/06/1996',TRUE,FALSE);
SELECT * FROM f_new_user('Ильдар','9519641129','bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a','CT1',1571943822,'09/06/1996',TRUE,FALSE);
SELECT id,username FROM users;

--Таблица drivers содержит инофрмацию о пользователе, как о водителе.
--Создается только после создания, один и тот же автомобиль не может быть
--привязан к разным людям, один человек = одн автомобиль.
--отвязать автомобиль является возможным, но после отвязки автомобиля 
--брать заявки можно будет только после привязки другого автомобиля.
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"rating" - рэйтин водителя
--"car" - машита водителя
--"active_session" - активная сессия
--"date_last_session" - дата активации последней сессии
DROP TABLE IF EXISTS drivers;
CREATE TABLE drivers (
id VARCHAR(15) REFERENCES users (id) ON DELETE CASCADE,
rating INTEGER NOT NULL DEFAULT 0,
car VARCHAR(15) DEFAULT '',
date_keep_car BIGINT NOT NULL DEFAULT 0,
car_arend VARCHAR(15) DEFAULT '',
doc_number VARCHAR(14) NOT NULL,
active_session VARCHAR(15) DEFAULT 'no',
verification_driver BOOLEAN DEFAULT FALSE,
UNIQUE(id),
UNIQUE(doc_number)
);
--Function add drivers
DROP FUNCTION IF EXISTS f_new_driver(_id VARCHAR(15),_username VARCHAR(30), _surname VARCHAR(30), _middlename VARCHAR(30), _doc_serial VARCHAR(6), _doc_number VARCHAR(8));
CREATE OR REPLACE FUNCTION f_new_driver(_id VARCHAR(15),_username VARCHAR(30), _surname VARCHAR(30), _middlename VARCHAR(30), _doc_serial VARCHAR(6), _doc_number VARCHAR(8)) RETURNS VARCHAR(15) AS $$
DECLARE
BEGIN
INSERT INTO drivers(id, doc_number) VALUES(_id, _doc_serial || ' ' || _doc_number);
UPDATE users SET username = _username, surname = _surname, middlename = _middlename WHERE id = _id;
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Function update driver
--Default:
SELECT f_new_driver('USR1','Василий','Гусельников','Иванович','1000','000000');
SELECT f_new_driver('USR2','Андрей','Кедо','Максимович','1000','000001');
SELECT * FROM drivers;

--Таблица tokens содержит в себе данные о токене и коде верефикации
--По полю date_of_token можно просмотреть последнюю активность на сервере
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"token" - уникальный верефикатор доступа, позволяет определить отправителя
--"date_of_token" - время замены токена
--"code_verefication" - код подтверждения
--"date_of_code" - дата замены кода верификации
DROP TABLE IF EXISTS tokens;
CREATE TABLE tokens (
id VARCHAR(15) REFERENCES users (id) ON DELETE CASCADE,
token VARCHAR(200) NOT NULL,
date_of_token BIGINT NOT NULL CHECK (date_of_token > 0),
fb_token VARCHAR(200),
data_of_fb_token BIGINT NOT NULL CHECK (data_of_fb_token > 0)
);

--Таблица rtokens содержит в себе данные о токене и коде верефикации
--По полю date_of_token можно просмотреть последнюю активность на сервере
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"token" - уникальный верефикатор доступа, позволяет определить отправителя
--"date_of_token" - время замены токена
--"code_verefication" - код подтверждения
--"date_of_code" - дата замены кода верификации
DROP TABLE IF EXISTS rtokens;
CREATE TABLE rtokens (
id VARCHAR(15) NOT NULL,
token VARCHAR(200),
date_of_token BIGINT CHECK (date_of_token > 0),
code_verification VARCHAR(6),
date_of_code BIGINT CHECK (date_of_code > 0),
token_firebase TEXT,
date_of_token_firebase BIGINT CHECK (date_of_code > 0)
);
--Function add sms in rtoken
DROP FUNCTION IF EXISTS f_new_rtokens_sms(_id_user VARCHAR(15), _code_verification VARCHAR(6), _date_of_code BIGINT);
CREATE OR REPLACE FUNCTION f_new_rtokens_sms(_id_user VARCHAR(15), _code_verification VARCHAR(6), _date_of_code BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
_exist_token VARCHAR(15);
BEGIN
_exist_token = '';
SELECT code_verification FROM rtokens WHERE id = _id_user INTO _exist_token;
IF _id_user == '' THEN
RETURN 0;
END IF;
IF _exist_token != '' THEN
UPDATE rtokens SET code_verification = _code_verification AND date_of_code = _date_of_code WHERE id_user = _id_user;
ELSE
INSERT INTO rtokens(id_user, code_verification, date_of_code) VALUES(_id_user, _code_verification, _date_of_code);
END IF;
RETURN _id_user;
END;
$$ LANGUAGE plpgsql;
--Function add token in rtoken
DROP FUNCTION IF EXISTS f_new_rtokens(_id_user VARCHAR(15), _token VARCHAR(200), _date_of_token BIGINT);
CREATE OR REPLACE FUNCTION f_new_rtokens(_id_user VARCHAR(15), _token VARCHAR(200), _date_of_token BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
_exist_token VARCHAR(15);
BEGIN
_exist_token = '';
SELECT _token FROM rtokens WHERE id = _id_user INTO _exist_token;
IF _id_user == '' THEN
RETURN 0;
END IF;
IF _exist_token != '' THEN
UPDATE rtokens SET token = _token AND date_of_token = _date_of_token WHERE id_user = _id_user;
ELSE
INSERT INTO rtokens(id_user, token, date_of_code) VALUES(_id_user, _token, _date_of_token);
END IF;
RETURN _id_user;
END;
$$ LANGUAGE plpgsql;
--Function add sms in rtoken
DROP FUNCTION IF EXISTS f_new_rtokens_firebase(_id_user VARCHAR(15), _token_firebase VARCHAR(200), _date_of_token_firebase BIGINT);
CREATE OR REPLACE FUNCTION f_new_rtokens_firebase(_id_user VARCHAR(15), _token_firebase VARCHAR(200), _date_of_token_firebase BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
_exist_token VARCHAR(15);
BEGIN
_exist_token = '';
SELECT token_firebase FROM rtokens WHERE id = _id_user INTO _exist_token;
IF _id_user == '' THEN
RETURN 0;
END IF;
IF _exist_token != '' THEN
UPDATE rtokens SET token_firebase = _token_firebase AND date_of_token_firebase = _date_of_token_firebase WHERE id_user = _id_user;
ELSE
INSERT INTO rtokens(id_user, token_firebase, date_of_token_firebase) VALUES(_id_user, _token_firebase, _date_of_token_firebase);
END IF;
RETURN _id_user;
END;
$$ LANGUAGE plpgsql;
--Таблица agents
DROP TABLE IF EXISTS agents;
CREATE TABLE agents(
id VARCHAR(15) NOT NULL PRIMARY KEY,
phone VARCHAR(10) NOT NULL,
pwd VARCHAR(64) NOT NULL,
username VARCHAR(30) NOT NULL,
surname VARCHAR(30) NOT NULL,
middlename VARCHAR(30),
city VARCHAR(15) REFERENCES cities (id) ON DELETE CASCADE,
date_reg BIGINT, 
date_bir VARCHAR(10),
block_agent BOOLEAN,
block_commit VARCHAR(256)
);
--Последовательность для таблицы: agents
DROP SEQUENCE IF EXISTS agents_id_seq;
CREATE SEQUENCE agents_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Function add new user: Регистрация нового пользователя в системе
DROP FUNCTION IF EXISTS f_new_agent(_username VARCHAR(30),_surname VARCHAR(30),_middlename VARCHAR(30),_phone VARCHAR(10),_pass VARCHAR(64),_city VARCHAR(15), _date_reg BIGINT, _date_bir VARCHAR(10));
CREATE OR REPLACE FUNCTION f_new_agent(_username VARCHAR(30),_surname VARCHAR(30),_middlename VARCHAR(30),_phone VARCHAR(10),_pass VARCHAR(64),_city VARCHAR(15), _date_reg BIGINT, _date_bir VARCHAR(10)) RETURNS VARCHAR(15) AS $$
DECLARE
_id_user VARCHAR(15);
_num_nextval VARCHAR(13);
BEGIN
_id_user = 'AG';
SELECT nextval('agents_id_seq') INTO _num_nextval;
_id_user = CONCAT(_id_user,_num_nextval);
INSERT INTO agents(id,phone,pwd,username,surname,middlename,city,date_reg,date_bir) VALUES(_id_user,_phone,_pass,_username,_surname,_middlename,_city,_date_reg,_date_bir);
RETURN _id_user;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_agent('Василий','Гусельников','Иванович','9825471689','bcb15f821479b4d5772bd0ca866c00ad5f926e3580720659cc80d39c9d09802a','CT1',19289382,'07/11/1986');

DROP TABLE IF EXISTS ptokens;
CREATE TABLE ptokens (
id VARCHAR(15) REFERENCES agents (id) ON DELETE CASCADE,
token VARCHAR(200) NOT NULL,
date_of_token BIGINT NOT NULL CHECK (date_of_token > 0),
fb_token VARCHAR(200),
data_of_fb_token BIGINT NOT NULL CHECK (data_of_fb_token > 0)
);

--Таблица owner_cabinet
--"own" - id админа (привязывается при администратором кабинета)
--"cabinet" - уникальный индетификатор. (Уникальное, внешний ключ)
DROP TABLE IF EXISTS owner_cabinet;
CREATE TABLE  owner_cabinet (
own VARCHAR(30) REFERENCES agents (id) ON DELETE SET NULL,
cabinet VARCHAR(15) REFERENCES cabinet_ps (id) ON DELETE SET NULL,
UNIQUE(own, cabinet)
);
--Default:
INSERT INTO owner_cabinet VALUES
('AG1','CBPS1'),
('AG1','CBPS2');
