# **FullDriver server authorization**  

Сервер для обработки запросов приложения **FullDriver** ```https://gitlab.com/three-developers/fulldriver```  
Для описания API сервера обратитесь в ```https://gitlab.com/three-developers/fdd_auth```  
Обратите внимание на ветку проекта, в запуск допускаются только master  

Документация:
https://gitlab.com/three-developers/fdd_auth/-/blob/master/doc/ServerDoc.md