package utility

import (
	"encoding/base64"
	"fmt"
	"strings"

	"github.com/valyala/fasthttp"
)

//Authorization - fuction for read header
func Authorization(ctx *fasthttp.RequestCtx) (username, password string, ok bool) {
	fmt.Println(ctx.Request.Header.ContentLength())
	auth := ctx.Request.Header.Peek("Authorization")
	if auth != nil {
		return parseBasic(string(auth))
	}
	auth = ctx.Request.Header.Peek("authorization")
	if auth != nil {
		return parseBasic(string(auth))
	}
	return
}

//parseBasic - fuction for parse login and password from header
func parseBasic(auth string) (username, password string, ok bool) {
	const prefix = "Basic "
	if !strings.HasPrefix(auth, prefix) {
		return
	}
	c, err := base64.StdEncoding.DecodeString(auth[len(prefix):])
	if err != nil {
		return
	}
	cs := string(c)
	s := strings.IndexByte(cs, ':')
	if s < 0 {
		return
	}
	return cs[:s], cs[s+1:], true
}
