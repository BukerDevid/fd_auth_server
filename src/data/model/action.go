package model

//ActionVerificationData -
type ActionVerificationData struct {
	Type   string           `json:"event"`
	Object string           `json:"subject"`
	Data   VerificationData `json:"data"`
}

//ActionNotifyData -
type ActionNotifyData struct {
	Type   string `json:"event"`
	Object string `json:"subject"`
}

type VerificationData struct {
	Verification bool   `json:"verification"`
	Comment      string `json:"comment"`
}

type ActionCreateOrderData struct {
	Type   string           `json:"event"`
	Object string           `json:"subject"`
	Data   OrderCreatedData `json:"data"`
}

type ActionLoadOrder struct {
	Type   string      `json:"event"`
	Object string      `json:"subject"`
	Data   interface{} `json:"data"`
}
