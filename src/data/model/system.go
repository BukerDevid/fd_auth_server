package model

//SessionAgentData - Data for agent session
type SessionAgentData struct {
}

//SessionAdminData - Data for admin session
type SessionAdminData struct {
}

//UserSystemModel -
type UserSystemModel struct {
	ID           string `json:"id"`
	Phone        string `json:"phone"`
	Name         string `json:"name"`
	Sex          string `json:"sex"`
	Verification bool   `json:"verification"`
	City         string `json:"city"`
}

//ListUserSystemModel -
type ListUserSystemModel struct {
	Count int               `json:"count"`
	List  []UserSystemModel `json:"list"`
}

//CarSystemModel -
type CarSystemModel struct {
	ID                string `json:"id"`
	RegistrationNmber string `json:"registration_number"`
	Mark              string `json:"mark"`
	Color             string `json:"color"`
	VerificationData  bool   `json:"verification"`
	Owner             string `json:"phone_owner"`
}

//ListElementForSystemModel -
type ListElementForSystemModel struct {
	Count int         `json:"count"`
	List  interface{} `json:"list"`
}
