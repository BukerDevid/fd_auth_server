package model

type SessionInformation struct {
	DataStart string          `json:"data_start"`
	DataEnd   string          `json:"data_end"`
	UnixStart int64           `json:"unix_start"`
	UnixEnd   int64           `json:"unix_end"`
	Factory   string          `json:"factory"`
	Status    string          `json:"status"`
	Amount    YandexPayAmount `json:"amount"`
}

type RegistrationPay struct {
	Token    string `json:"token"`
	Session  string `json:"session"`
	Promocod string `json:"promocod"`
}

type CreatePay struct {
	Type   string   `json:"type"`
	Event  string   `json:"event"`
	Object PayItems `json:"object"`
}

type Payment struct {
	Token   string `json:"token"`
	Payment string `json:"payment"`
	TypePay string `json:"type"`
}

type PayItems struct {
	Title                string          `json:"title"`
	Subect               string          `json:"subject"`
	ClientApplicationKey string          `json:"clientApplicationKey"`
	ShopID               string          `json:"shopId"`
	Amount               YandexPayAmount `json:"amount"`
}

type YandexNotifyPay struct {
	Type   string          `json:"type"`
	Event  string          `json:"event"`
	Object YandexPayObject `json:"object"`
}

type YandexPayObject struct {
	ID            string           `json:"id"`
	Status        string           `json:"status"`
	Paid          bool             `json:"paid"`
	Amount        YandexPayAmount  `json:"amount"`
	AuthDetals    interface{}      `json:"authorization_details"`
	CreatedAT     string           `json:"created_at"`
	Description   string           `json:"description"`
	ExpiresAT     string           `json:"expires_at"`
	MetaDate      interface{}      `json:"metadata"`
	PaymentMethod YandexPatPayment `json:"payment_method"`
	Refund        bool             `json:"refundable"`
	Test          bool             `json:"test"`
}

// type YandexPayAnswer struct {}

type YandexPayAmount struct {
	Value    string `json:"value"`
	Currency string `json:"currency"`
}

type YandexPatPayment struct {
	Type  string      `json:"type"`
	ID    string      `json:"id"`
	Saved string      `json:"saved"`
	Card  interface{} `json:"card"`
	Title string      `json:"title"`
}

type YandexPayReceipt struct {
	Customer YandexPayCostumer `json:"customer"`
	Items    []YandexPayItem   `json:"items"`
	Phone    string            `json:"phone"`
}

type YandexPayCostumer struct {
	FullName string `json:"full_name"`
	Phone    string `json:"phone"`
}

type YandexPayItem struct {
	Description    string          `json:"description"`
	Quantity       string          `json:"quantity"`
	Amount         YandexPayAmount `json:"amount"`
	VatCode        int             `json:"vat_code"`
	PaymentSubject string          `json:"payment_subject"`
	PaymentMode    string          `json:"payment_mode"`
}

type YandexPayConfirmation struct {
	Type string `json:"type"`
	// Locale string `json:"locale"`
	Enforce   bool   `json:"enforce"`
	ReturnURL string `json:"return_url"`
}

type YandexPayMethod struct {
	Type string `json:"type"`
}

type YandexPayment struct {
	Amount  YandexPayAmount  `json:"amount"`
	Receipt YandexPayReceipt `json:"receipt"`
	// PaymentMethod YandexPayMethod       `json:"payment_method_data"`
	Confirmation YandexPayConfirmation `json:"confirmation"`
	PaymentToken string                `json:"payment_token"`
	Capture      bool                  `json:"capture"`
	Description  string                `json:"description"`
}

// "amount": {//Сумма платежа
// 	"value": string //Сумма оплаты
// 	"currency": string //RUB
// }
// "receipt": { //Данные для формирования чека
// 	"customer": { //Информация о пользователе
// 		"full_name": string //фио
// 		"phone": string //телефон пользователя
// 		//inn - ИНН игнорируется
// 		//email - игнорируется
// 	}
// 	"items": [ //Список товаров
// 		{
// 			"description": string
// 			"quantity": string
// 			"amount": { //Дублируется amount
// 				"value": string //Сумма оплаты
// 				"currency": string //RUB
// 			}
// 			"vat_code": int //НДС (1-Без %)
// 			"payment_subject": string //Признак предмета расчета service
// 			"payment_mode": string //Признак способа расчета full_payment (Полная оплата)
// 			//product_code - игнорируется
// 			//country_of_origin_code - игнорируется
// 			//customs_declaration_number - игнорируется
// 			//excise - игнорируется
// 		}
// 	]
// 	"phone": string //Телефон пользователя дублируется
// 	//email - игнорируется
// }
// //recipient - игнорируется
// "payment_token": string //Токен оплаты
// //payment_method_id - игнорируется
// //payment_method_data - игнорируется
// "confirmation": { //Даные для инициализации оплаты
// 	"type": string //Код сценария подтверждения redirect/embedded/external
// 	"locale": string //Язык интерфейса пользователя ru_RU
// }
// //save_payment_method - игнорируется
// "capture": bool //Автоматический платеж true
// //client_ip - игнорируется
// //metadata - игнорируется
// //airline - игнорируется
// //transfers - игнорируется
// "description": string //Описание

//SELECT tr.id, tr.date_tr, tr.session_dr, tr.summ, ss.duration, rg.utc FROM (SELECT id, MAX(date_tr) AS date_tr, session_dr, driver, summ FROM transaction_dr WHERE driver = 'USR7' GROUP BY id, date_tr, session_dr, driver, summ LIMIT 1) AS tr, sessions_dr AS ss, users AS us, cities AS ct, regions AS rg WHERE ss.id = tr.session_dr AND us.id = tr.driver AND ct.id = us.city AND rg.id = ct.region;
