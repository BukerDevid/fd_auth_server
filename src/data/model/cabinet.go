package model

//GetCabinetDataDriver -
type GetCabinetDataDriver struct {
	ID    string  `json:"id"`
	Title string  `json:"title"`
	Lat   float64 `json:"lat"`
	Lon   float64 `json:"lon"`
	Addr  string  `json:"addr"`
}

//GetCarDataFromCabinetDriver -
type GetCarDataFromCabinetDriver struct {
	ID          string    `json:"id"`
	Color       ColorAuto `json:"color"`
	Mark        string    `json:"mark"`
	NumReg      string    `json:"numreg"`
	IDCabinet   string    `json:"cabinet_id"`
	NameCabinet string    `json:"name"`
}

//SetAgentData -
type SetAgentData struct {
	Token      string `json:"token"`
	Pwd        string `json:"pwd"`
	Phone      string `json:"phone"`
	Username   string `json:"username"`
	Surname    string `json:"surname"`
	Middlename string `json:"middlename"`
	City       string `json:"city"`
	DateBir    string `json:"date-bir"`
}

//SetCabinetDataData -
type SetCabinetDataData struct {
	Token    string  `json:"token"`
	Type     string  `json:"type"`
	TitleOut string  `json:"title_out"`
	TitleIn  string  `json:"title_in"`
	City     string  `json:"city"`
	Lat      float64 `json:"lat"`
	Lon      float64 `json:"lon"`
	Addr     string  `json:"addr"`
}

//SetSessionData -
type SetSessionData struct {
	Token    string  `json:"token"`
	Cabinet  string  `json:"cabinet"`
	Duration string  `json:"duration"`
	Price    float64 `json:"price"`
}

//GetSessionData -
type GetSessionData struct {
	ID       string  `json:"id"`
	Cabinet  string  `json:"cabinet"`
	Duration string  `json:"duration"`
	Price    float64 `json:"price"`
	Active   bool    `json:"active"`
}

//CabinetData -
type CabinetData struct {
	ID         string `json:"id"`
	TitleOut   string `json:"title_out"`
	TitleIn    string `json:"title_in"`
	Del        bool   `json:"deleted"`
	DelComment string `json:"del_comment"`
	Limit      bool   `json:"limit"`
	LimitNum   int    `json:"limit_num"`
	City       string `json:"city"`
	Lat        string `json:"lat"`
	Lon        string `json:"lon"`
	Addr       string `json:"addr"`
}

//SessionForDrivers -
type SessionForDrivers struct {
	ID       string  `json:"id"`
	Cabinet  string  `json:"cabinet"`
	Duration int     `json:"duration"`
	Price    float64 `json:"price"`
	Promocod bool    `json:"promocod"`
}
