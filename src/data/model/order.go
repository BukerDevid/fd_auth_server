package model

//CreateOrder -
type CreateOrder struct {
	ID       string      `json:"id"`
	Username string      `json:"nme"`
	Places   []Place     `json:"plc"`
	InCity   bool        `json:"typ"`
	Option   OptionModel `json:"opt"`
}

//Place - Data Place for order
type Place struct {
	City      string  `json:"city"`
	Addr      string  `json:"addr"`
	TypePorch int     `json:"type_porch"`
	Porch     int     `json:"porch"`
	Lat       float64 `json:"lat"`
	Lon       float64 `json:"lon"`
}

//OptionModel  - for OrderModelForSend
type OptionModel struct {
	Price     int    `json:"price"`
	Babych    bool   `json:"babych"`
	Smoking   bool   `json:"smoking"`
	FastUp    bool   `json:"fastup"`
	Comment   string `json:"comment"`
	Animal    bool   `json:"animal"`
	PlanOrder bool   `json:"plan_order"`
	PlanTime  int    `json:"time_plan"`
	SystemPay bool   `json:"pay"`
}

//ReadyOrderModelClient - client data
type ReadyOrderModelClient struct {
	Actor     string          `json:"actor"`
	ID        string          `json:"sending"`
	IDOrder   string          `json:"order"`
	InCity    bool            `json:"in-city"` //True - in City/False - out City
	Places    []Place         `json:"places"`
	AddOption OptionData      `json:"parametrs"`
	Driver    DriverOrderData `json:"driver"`
}

type ReadyOrderModelCreated struct {
	Actor     string     `json:"actor"`
	ID        string     `json:"sending"`
	IDOrder   string     `json:"order"`
	InCity    bool       `json:"in-city"` //True - in City/False - out City
	Places    []Place    `json:"places"`
	AddOption OptionData `json:"parametrs"`
}

//ReadyOrderModelDriver - driver data
type ReadyOrderModelDriver struct {
	Actor     string          `json:"actor"`
	ID        string          `json:"sending"`
	IDOrder   string          `json:"order"`
	InCity    bool            `json:"in-city"` //True - in City/False - out City
	Places    []Place         `json:"places"`
	AddOption OptionData      `json:"parametrs"`
	Client    ClientOrderData `json:"client"`
}

//ClientOrderData - Data Client for Order
type ClientOrderData struct {
	IDClient string `json:"iduser"`
	Name     string `json:"username"`
	Rating   int    `json:"rating"`
	Phone    string `json:"phone"`
	Img      string `json:"img"`
}

//DriverOrderData - Data Driver for Order
type DriverOrderData struct {
	IDDriver string           `json:"iddriver"`
	Name     string           `json:"username"`
	Phone    string           `json:"phone"`
	Duration int              `json:"duration"`
	Rating   int              `json:"rating"`
	Img      string           `json:"img"`
	Car      CarOrderData     `json:"car"`
	Partner  PartnerOrderData `json:"partner"`
}
