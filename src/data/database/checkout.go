package database

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"../model"
	"github.com/shopspring/decimal"
)

//CreatePayInDatabase - annonce pay in database
func (db *Database) CreatePayInDatabase(payData model.RegistrationPay, idUser string) ([]byte, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var peymentConcept model.CreatePay
	var title string
	var duration, price, itog, deduction int
	var proc float64
	var promo bool
	peymentConcept.Event = "ok"
	peymentConcept.Type = "registration"
	peymentConcept.Object.ClientApplicationKey = "live_Njc2MTE4kPdJfHgBgOzPfB1095W1ZZqojmB8FHCFBUY"
	peymentConcept.Object.ShopID = "676118"
	if err := dbConn.QueryRow(context.Background(),
		"SELECT cb.title_ps, sdr.duration, sdr.price, CASE WHEN bool_or(cb.id like '%FD%') THEN TRUE ELSE FALSE END AS promo FROM sessions_dr AS sdr, cabinet_ps AS cb WHERE cb.id = sdr.cabinet AND sdr.active = TRUE AND sdr.id = $1 GROUP BY cb.title_ps, sdr.duration, sdr.price",
		payData.Session).Scan(&title, &duration, &price, &promo); err != nil {
		fmt.Println("Error - ", err.Error())
		return nil, err
	}
	peymentConcept.Object.Title = fmt.Sprintf("Сессия %v", title)
	itog = price
	duration = duration / 60
	peymentConcept.Object.Subect = fmt.Sprintf("Дительность: %v мин \nСтоимость: %v руб.", duration, price)
	if len(payData.Promocod) > 6 && promo {
		unix := time.Now().Unix()
		var id string
		if err := dbConn.QueryRow(context.Background(),
			"SELECT id, deduction FROM promo_cod WHERE cods = $1 AND time_out > $2 AND count_use != 0",
			payData.Promocod, unix).Scan(&id, &deduction); err != nil {
			fmt.Println("Error - ", err.Error())
			id = ""
			deduction = 0
		}
		if deduction > 0 {
			fmt.Println(price, deduction, proc)
			proc = (float64(price) / 100) * float64(deduction)
			itog = price - int(proc)
		}
	}
	if itog != price {
		if itog == 0 {
			peymentConcept.Event = "null"
		}
		peymentConcept.Object.Subect = fmt.Sprintf("%v\n Cкидка: %v руб. \nИТОГО: %v руб.", peymentConcept.Object.Subect, proc, itog)
	} else {
		peymentConcept.Object.Subect = fmt.Sprintf("%v\n ИТОГО: %v руб.", peymentConcept.Object.Subect, itog)
	}
	peymentConcept.Object.Amount.Value = fmt.Sprintf("%v.00", itog)
	peymentConcept.Object.Amount.Currency = "RUB"
	//create in database
	unix := time.Now().Unix()
	if len(payData.Promocod) > 6 {
		if peymentConcept.Event == "null" {
			dbConn.Exec(context.Background(),
				"SELECT f_new_transaction_dr($1,$2,$3,$4,$5,$6)",
				unix, payData.Session, idUser, itog, payData.Promocod, "SUCCEEDED")
		} else {
			dbConn.Exec(context.Background(),
				"SELECT f_new_transaction_dr($1,$2,$3,$4,$5,$6)",
				unix, payData.Session, idUser, itog, payData.Promocod, "ANNOUNCED")
		}
	} else {
		dbConn.Exec(context.Background(),
			"SELECT f_new_transaction_dr($1,$2,$3,$4,$5,$6)",
			unix, payData.Session, idUser, itog, "", "ANNOUNCED")
	}
	data, err := json.Marshal(peymentConcept)
	if err != nil {
		fmt.Println("Error - ", err.Error())
		return nil, err
	}
	return data, nil
}

//SessionInformationFromDatabase -
func (db *Database) SessionInformationFromDatabase(idUser string) ([]byte, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var err error
	var session model.SessionInformation
	var summ decimal.Decimal
	var duration, utc int
	var idPay string
	var unixStart int64
	if err = dbConn.QueryRow(context.Background(),
		"SELECT tr.id, tr.date_tr, tr.summ, tr.status_pay, ss.duration, rg.utc, cb.title_ps FROM (SELECT id, date_tr, session_dr, driver, summ, status_pay FROM transaction_dr WHERE driver = $1 GROUP BY id, date_tr, session_dr, driver, summ ORDER BY date_tr DESC LIMIT 1) AS tr, sessions_dr AS ss, cabinet_ps AS cb, users AS us, cities AS ct, regions AS rg WHERE ss.id = tr.session_dr AND cb.id = ss.cabinet AND us.id = tr.driver AND ct.id = us.city AND rg.id = ct.region",
		idUser).Scan(&idPay, &unixStart, &summ, &session.Status, &duration, &utc, &session.Factory); err != nil { //  id, date_tr, session_dr, summ, duration, utc
		return nil, err
	}
	session.UnixStart = unixStart + int64(utc*3600)
	session.UnixEnd = unixStart + int64(utc*3600) + int64(duration)
	session.DataStart = time.Unix(unixStart+int64(utc*3600), 0).Format("15:04-02/01/2006")
	session.DataEnd = time.Unix(unixStart+int64(duration)+int64(utc*3600), 0).Format("15:04-02/01/2006")
	if session.Status == "ANNOUNCED" {
		return nil, nil
	}
	if time.Now().Unix() > unixStart+int64(duration) && (session.Status == "SUCCEEDED") {
		dbConn.Exec(context.Background(), "UPDATE transaction_dr SET status_pay = $1, date_st = $2 WHERE id = $3", "TIME_OUT", time.Now().Unix(), idPay)
		session.Status = "TIME_OUT"
	}
	session.Amount.Value = fmt.Sprintf("%s.00", summ.String())
	session.Amount.Currency = "RUB"
	data, err := json.Marshal(session)
	if err != nil {
		return nil, err
	}
	return data, nil
}

//SendPay -
func (db *Database) SendPay(idUser, tokenYandex, typePay string) ([]byte, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, err
	}
	var idPay, idSess, status, phone, name, url string
	var duration int
	var summ decimal.Decimal
	if err = dbConn.QueryRow(context.Background(),
		"SELECT id, session_dr, summ, status_pay FROM transaction_dr WHERE driver = $1 ORDER BY date_tr DESC LIMIT 1",
		idUser).Scan(&idPay, &idSess, &summ, &status); err != nil {
		return nil, err
	}
	if status == "ANNOUNCED" {
		dbConn.QueryRow(context.Background(),
			"SELECT * FROM (SELECT surname || ' ' || username || ' ' || middlename AS full_name, phone FROM users WHERE id = $1) AS us, (SELECT duration FROM sessions_dr WHERE id = $2) AS ss",
			idUser, idSess).Scan(&name, &phone, &duration)
		if url, err = db.payCheckout(idPay, name, phone, tokenYandex, typePay, summ, duration); err != nil {
			return nil, err
		}
	}
	var URL = model.One{}
	URL.Value = url
	data, err := json.Marshal(URL)
	return data, nil
}

func (db *Database) payCheckout(idPay, name, phone, tokenYandex, typePay string, summ decimal.Decimal, duration int) (string, error) {
	var Payment = model.YandexPayment{}
	Payment.Amount.Value = summ.String()     //Стоимость
	Payment.Amount.Currency = "RUB"          //Валюта
	Payment.Receipt.Customer.FullName = name //Имя пользователя
	Payment.Receipt.Customer.Phone = phone   //Телефон пользователя
	Item := model.YandexPayItem{}
	Item.Description = fmt.Sprintf("Подписка на сервис FullDriver. Длительность %v мин.", duration/60)
	Item.Quantity = "1.00"
	Item.Amount.Value = summ.String() //Стоимость
	Item.Amount.Currency = "RUB"      //Валюта
	Item.VatCode = 1
	Item.PaymentSubject = "service"
	Item.PaymentMode = "full_payment"
	Payment.Receipt.Items = append(Payment.Receipt.Items, Item)
	Payment.Receipt.Phone = phone
	// Payment.PaymentMethod.Type = typePay
	// Payment.PaymentMethod.Type = "bank_card"
	Payment.Confirmation.Type = "redirect"
	Payment.Confirmation.Enforce = false
	Payment.Confirmation.ReturnURL = "https://fulldriver.su:4777/fd/pay"
	// Payment.Confirmation.Locale = "ru_RU"
	Payment.PaymentToken = tokenYandex
	Payment.Description = fmt.Sprintf("Подписка на сервис FullDriver. Длительность %v мин.", duration/60)
	// Payment.Test = true
	Payment.Capture = true
	data, err := json.Marshal(Payment)
	if err != nil {
		return "", err
	}
	fmt.Println(string(data))
	client := &http.Client{}
	req, err := http.NewRequest("POST", "https://payment.yandex.net/api/v3/payments", bytes.NewBuffer(data))
	req.SetBasicAuth("676118", "live_5JKKMnwwnJlbdKyXImsfjETx-MlzfJKuQeXyfkQIX6M")
	req.Header.Add("Idempotence-Key", idPay)
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		switch resp.StatusCode {
		case 401:
			return "", errors.New(string("Yandex.checkout (401) - error authorithation."))
		case 404:
			return "", errors.New(string("Yandex.checkout (404) - error server."))
		default:
			return "", errors.New(string(fmt.Sprintf("Yandex.checkout - error %v", resp.StatusCode)))
		}
	}
	// fmt.Println("Status 200 Success Pay")
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var dataAnswer map[string]interface{}
	answ, err := ioutil.ReadAll(resp.Body)
	fmt.Println("All body", string(answ))
	err = json.Unmarshal(answ, &dataAnswer)
	if err != nil {
		fmt.Println(err.Error())
		return "", nil
	}
	dbConn.Exec(context.Background(), "UPDATE transaction_dr SET yandex_id = $1, status_pay = $2 WHERE id = $3", dataAnswer["id"].(string), "WAITING", idPay)
	if dataAnswer["test"].(bool) {
		fmt.Println("Test Pay - success!")
	} else {
		fmt.Println("Real Pay - success!")
	}
	if confirm, ok := dataAnswer["confirmation"]; ok {
		var dataConfirm map[string]interface{}
		// err = json.Unmarshal(confirm, &dataConfirm)
		dataConfirm, ok = confirm.(map[string]interface{})
		if !ok {
			fmt.Println("Error confirm read")
			return "", nil
		}
		if url, ok := dataConfirm["confirmation_url"]; ok {
			if urlStr, ok := url.(string); ok {
				fmt.Println("Confirm url: ", urlStr)
				return urlStr, nil
			} else {
				fmt.Println("Error return_url convert tu string")
			}
		} else {
			fmt.Println("Error return_url")
		}
	}
	return "", nil
}

//PayStatusHandler - handler for check status paymant from yandex check-out
func (db *Database) PayStatusHandler() {
	for {
		db.ChangePayStatus()
		time.Sleep(time.Second * 5)
	}
}

//ChangePayStatus -
func (db *Database) ChangePayStatus() {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return
	}
	dbConnSec, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConnSec.Release()
	if err != nil {
		return
	}
	var idYandex, status, url string
	query, err := dbConn.Query(context.Background(), "SELECT yandex_id FROM transaction_dr WHERE status_pay = 'WAITING'")
	if err != nil {
		return
	}
	for query.Next() {
		query.Scan(&idYandex)
		client := &http.Client{}
		url = fmt.Sprintf("https://payment.yandex.net/api/v3/payments/%v", idYandex)
		fmt.Println(url)
		req, err := http.NewRequest("GET", url, nil)
		req.SetBasicAuth("676118", "live_5JKKMnwwnJlbdKyXImsfjETx-MlzfJKuQeXyfkQIX6M")
		resp, err := client.Do(req)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			switch resp.StatusCode {
			case 401:
				fmt.Println("Yandex.checkout (401) - error authorithation.")
				continue
			case 404:
				fmt.Println("Yandex.checkout (404) - error server.")
				continue
			default:
				fmt.Println("Yandex.checkout - error ", resp.StatusCode)
				answ, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					continue
				}
				fmt.Println("All body", string(answ))
				continue
			}
		}

		var dataAnswer map[string]interface{}
		answ, err := ioutil.ReadAll(resp.Body)
		fmt.Println("All body", string(answ))
		err = json.Unmarshal(answ, &dataAnswer)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		if _, ok := dataAnswer["id"]; ok {
			if dataAnswer["id"] != idYandex {
				continue
			}
			if _, ok := dataAnswer["status"]; !ok {
				continue
			}
			switch dataAnswer["status"] {
			case "waiting_for_capture":
				status = "WAITING"
			case "succeeded":
				status = "SUCCEEDED"
			case "canceled":
				status = "CANCELED"
			case "pending":
				continue
			default:
				fmt.Println("Yandex.set_satate uknow status: ", dataAnswer["status"])
				continue
			}
			unix := time.Now().Unix()
			if status == "SUCCEEDED" {
				if err = dbConnSec.QueryRow(context.Background(),
					"UPDATE transaction_dr SET status_pay = $1, date_st = $3, date_tr = $3 WHERE yandex_id = $2 RETURNING id;",
					status, idYandex, unix).Scan(&idYandex); err != nil {
					continue
				}
			} else {
				if err = dbConnSec.QueryRow(context.Background(),
					"UPDATE transaction_dr SET status_pay = $1, date_st = $3 WHERE yandex_id = $2 RETURNING id;",
					status, idYandex, unix).Scan(&idYandex); err != nil {
					continue
				}
			}
			fmt.Println("Status update - ", idYandex)
		}
	}
	return
}

//RefundPay -
func (db *Database) RefundPay(payInfo model.YandexNotifyPay) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return err
	}
	var scanID string
	if err = dbConn.QueryRow(context.Background(),
		"UPDATE transaction_dr SET status_pay = $1 WHERE id = $2 RETURNING id;",
		"REFUND", payInfo.Object.ID).Scan(&scanID); err != nil {
		return err
	}
	fmt.Println("Status update - ", payInfo.Object.ID, scanID)
	//update status send FB
	return nil
}
