package database

/*
Packet database for client API.
Author: GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru
*/

import (
	"encoding/json"
	"fmt"
	"net/http"

	"../../sms_server"
	"../../utility"
	"../firebase"
	"../model"
)

/*AdminRepo - interface for admin*/
var errorRepo = "Repository error# "

//ClientRepo - is a interface using for client
type ClientRepo interface {
	CheckAuthUser(phone, pwd string) (string, int, error)
	CheckUserInDatabase(phone string) bool
	GetSms(phone string) (string, error)
	TakeSmsForCheck(phone string) ([]byte, error)
	CheckSmsKey(phone string, key string) (bool, error)
	LoadListCities() ([]byte, error)
	LoadListAllCities() ([]byte, error)
	CityNameByEnCityName(cityEn string) (bool, string, error)
	LoadListColor() ([]byte, error)
	LoadListMark() ([]byte, error)
	LoadStart() ([]byte, error)
	TakeIDUser(phone string) (string, error)
	TakeIDCar(idUser string, typeCar bool) (string, error)
	TakeToken(idUser, fbToken string) (string, error)
	CheckRegistrationToken(token string) (bool, string, error)
	CheckExist(registrationNumber string) (bool, error)
	CheckOwnerCar(registrationNumber string) (bool, error)
	CheckToken(token string) (bool, string, error)
	CheckCity(city string) bool
	TakeRegistrationToken(phone string) (string, error)
	KeepOwnerUserToCar(idCar, idUser string) (string, int, error)
	KeepCarInDatabase(idUser string, CarData model.SetCarData) error
	KeepoutCarInDatabase(idUser string) error
	RegistrationUserInDatabase(userData model.SetUserData) (string, error)
	RegistrationDriverInDatabase(driverData model.SetDriverData, idUser string) (string, int, error)
	RegistrationCarInDatabase(carData model.SetCarData, idUser, fbToken string) ([]byte, int, error)
	RecoveryPassInDatabase(phone, pass string) error
	RemoveImageUserInDatabase(idUser string) error
	SetImageUserInDatabase(idUser string) (string, error)
	UpdateUserInDatabase(idUser string, modelUUser model.UpdateUserData) error
	UpdateCarInDatabase(idUser string, CarData model.SetCarData) (string, error)
	DeleteUserInDatabase(idUser string) error
	DeleteCarInDatabase(idOwner string) bool
	RecoveryUserInDatabase(idUser string) error
	DeleteRegistrationToken(phone string /*phone*/) error
	DeleteToken(token, idUser string /*token or id*/) error
	WriteSms(phone, token string)
	SwitchModeDriver(idUser string) error
	TakeIDCarOnRegistrationNumber(registrationNumber string) (string, error)
	SessionData(idUser, fbToken string) ([]byte, error)
	GetListCabinetInCity(idUser string) ([]model.GetCabinetDataDriver, error)
	GetListCheckRendAuto(idUser string) ([]model.GetCarDataFromCabinetDriver, error)
	KeepAutoInDatabase(idUser, idCar, idCabinet string) bool
	KeepOutAutoInDatabase(idUser string) bool
	GetSessionForDriver(idCity string) ([]model.SessionForDrivers, error)
	TakeIDCityUser(idUser string) (string, error)
	CreatePayInDatabase(payData model.RegistrationPay, idUser string) ([]byte, error)
	SessionInformationFromDatabase(idUser string) ([]byte, error)
	SendPay(idUser, tokenYandex, typePay string) ([]byte, error)
	RefundPay(payInfo model.YandexNotifyPay) error
}

//DriverRepo - is a interface using for driver
type DriverRepo interface {
	GetDriverData(idUser string) (model.GetDriverData, error)
}

//AuthUser - back user data
func AuthUser(repo ClientRepo, phone, pwd, fbToken string) ([]byte, int, error) {
	idUser, check, err := repo.CheckAuthUser(phone, pwd)
	if err != nil {
		return nil, check, utility.ErrorHandler(errorRepo+": CheckAuth", err)
	}
	if check != 0 {
		return nil, check, nil
	}
	// data, err := repo.GetUserData(idUser, fbToken)
	data, err := repo.SessionData(idUser, fbToken)
	if data == nil {
		fmt.Println("Data - null")
		return data, check, nil
	}
	return data, check, nil
}

//CheckAuth - back user data
func CheckAuth(repo ClientRepo, phone, pwd, fbToken string) ([]byte, int, error) {
	var modelToken model.Token
	fmt.Println("Check auth user: ", phone, " ", pwd)
	idUser, check, err := repo.CheckAuthUser(phone, pwd)
	if check != 0 {
		return nil, check, nil
	}
	if err != nil {
		return nil, check, utility.ErrorHandler(errorRepo+": TakeTken", err)
	}
	modelToken.Token, err = repo.TakeToken(idUser, fbToken)
	if err != nil {
		return nil, check, utility.ErrorHandler(errorRepo+": TakeTken", err)
	}
	data, err := json.Marshal(modelToken)
	if err != nil {
		return nil, check, utility.ErrorHandler(errorRepo+": MarshalJSON", err)
	}
	return data, check, nil
}

//RegistrationUser - registration driver in database
func RegistrationUser(repo ClientRepo, ctx *http.Request) ([]byte, int, error) {
	var data []byte
	ctx.Body.Read(data)
	modelUserData := model.SetUserData{}
	err := json.NewDecoder(ctx.Body).Decode(&modelUserData)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorJSON+": NewDecoder", err)
	}
	if check := repo.CheckUserInDatabase(modelUserData.Phone); check == true {
		return nil, 1, nil
	}
	check, phone, err := repo.CheckRegistrationToken(modelUserData.Token)
	if check != true {
		return nil, 0, utility.ErrorHandler(errorPQX+": CheckRegistration", err)
	}
	repo.DeleteRegistrationToken(phone)
	fbToken := ctx.URL.Query().Get("fb")
	idUser, err := repo.RegistrationUserInDatabase(modelUserData)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorRepo+": RegistrationUser", err)
	}
	fmt.Println("Ok registration user in database " + idUser)
	data, err = repo.SessionData(idUser, fbToken)
	return data, 0, err
}

//RegistrationDriver - registration driver in database
func RegistrationDriver(repo ClientRepo, ctx *http.Request) ([]byte, int, error) {
	var data []byte
	ctx.Body.Read(data)
	modelDriverData := model.SetDriverData{}
	err := json.NewDecoder(ctx.Body).Decode(&modelDriverData)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorJSON+": RegistrationDriver.NewDecoder", err)
	}
	fbToken := ctx.URL.Query().Get("fb")
	check, idUser, err := repo.CheckToken(modelDriverData.Token)
	if check != true {
		return nil, 3, nil
	}
	idUser, res, err := repo.RegistrationDriverInDatabase(modelDriverData, idUser)
	if res == 1 || res == 2 {
		return nil, res, nil
	}
	fmt.Println("Ok registration driver in database " + idUser)
	data, err = repo.SessionData(idUser, fbToken)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorPQX+": RegistrationDriver.GetDriverData", err)
	}
	return data, 0, err
}

//RegistrationCar - Registration car in database
func RegistrationCar(repo ClientRepo, ctx *http.Request) ([]byte, int, error) {
	var data []byte
	var res int
	ctx.Body.Read(data)
	modelSetCarData := model.SetCarData{}
	err := json.NewDecoder(ctx.Body).Decode(&modelSetCarData)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorJSON+": RegistrationCar.NewDecoder", err)
	}
	fbToken := ctx.URL.Query().Get("fb")
	check, idUser, err := repo.CheckToken(modelSetCarData.Token)
	if check != true {
		return nil, 3, utility.ErrorHandler(errorPQX+": RegistrationCar.CheckRegistrationToken", err)
	}
	check, err = repo.CheckExist(modelSetCarData.NumReg)
	if err != nil {
		return nil, 0, err
	}
	if check == true {
		fmt.Println("Car exist")
		check, err = repo.CheckOwnerCar(modelSetCarData.NumReg)
		if err != nil {
			return nil, 0, err
		}
		if check == true {
			return nil, 4, nil
		}
		idCar, _ := repo.UpdateCarInDatabase(idUser, modelSetCarData)
		if idCar == "" {
			return nil, 2, nil
		}
		fmt.Println("Car by Update! it's  cat with id = ", idCar)
		_, res, err = repo.KeepOwnerUserToCar(idCar, idUser)
		if err != nil {
			return nil, 0, err
		}
		if res == 1 || res == 2 { // 1 - exists, 2 - Unknow error
			return nil, res, nil
		}
		fmt.Println("Keep Success!")
		data, err = repo.SessionData(idUser, fbToken)
		if err != nil {
			return nil, 0, err
		}
		fmt.Println("Success Get Cars Data")
	} else {
		data, res, err = repo.RegistrationCarInDatabase(modelSetCarData, idUser, fbToken)
		if res == 1 || res == 2 { // 1 - exists, 2 - Unknow error
			return nil, res, nil
		}
		if err != nil {
			return nil, 0, utility.ErrorHandler(errorPQX+": RegistrationCar.GetDriverData", err)
		}
	}
	return data, 0, err
}

//CallSms - call user sms
func CallSms(repo ClientRepo, sms *sms_server.SMS, phone string) error {
	key, err := repo.GetSms(phone)
	if err != nil {
		return utility.ErrorHandler(" CallSms.GetSms ", err)
	}
	err = sms.WriteSms(phone, key)
	return err
}

//WriteSms - write sms code in push
func (db *Database) WriteSms(phone, token string) {
	fmt.Println("Send key verification...")
	err := firebase.SendTestMessage(token, phone)
	if err != nil {
		fmt.Println(err.Error())
	}
}

//CallSmsDevel - take user sms for check FOR DEVELOPERS
func CallSmsDevel(repo ClientRepo, phone string) ([]byte, error) {
	data, err := repo.TakeSmsForCheck(phone)
	return data, err
}

//CheckSms - back user data
func CheckSms(repo ClientRepo, phone, key string) ([]byte, error) {
	var modelToken model.Token
	check, err := repo.CheckSmsKey(phone, key)
	if check != true {
		return nil, err
	}
	modelToken.Token, err = repo.TakeRegistrationToken(phone)
	if err != nil {
		return nil, err
	}
	data, err := json.Marshal(modelToken)
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": CheckSms.Marshal", err)
	}
	return data, err
}

//CheckingUser - back user data
func CheckingUser(repo ClientRepo, phone string) bool {
	return repo.CheckUserInDatabase(phone)
}

//CheckingUserToken - Cheking user id by token
func CheckingUserToken(repo ClientRepo, token string) (bool, string, error) {
	return repo.CheckToken(token)
}

//ListCities - back user data
func ListCities(repo ClientRepo) ([]byte, error) {
	return repo.LoadListAllCities()
}

//ListAllCities - back user data
func ListAllCities(repo ClientRepo) ([]byte, error) {
	return repo.LoadListCities()
}

//ListColors - back user data
func ListColors(repo ClientRepo) ([]byte, error) {
	return repo.LoadListColor()
}

//ListMarks - back user data
func ListMarks(repo ClientRepo) ([]byte, error) {
	return repo.LoadListMark()
}

//CheckCityEn - Loaf name city for checking in geo_modul
func CheckCityEn(repo ClientRepo, cityEn string) (bool, string, error) {
	return repo.CityNameByEnCityName(cityEn)
}

//ListStartData - back user data
func ListStartData(repo ClientRepo) ([]byte, error) {
	return repo.LoadStart()
}

//TestRepo - take of data after success authorization
func TestRepo(repo ClientRepo) error {
	fmt.Println("Check repo!")
	return nil
}

//UpdateUserData - update user data: Phone, Name, Date Birthday, Password
func UpdateUserData(repo ClientRepo, ctx *http.Request, modelUUser model.UpdateUserData) ([]byte, int, error) {
	if len(modelUUser.Token) < 120 {
		fmt.Println("Error token...")
		return nil, 1, nil
	}
	fbToken := ctx.URL.Query().Get("fb")
	var check bool
	var idUser string
	var checkReg bool
	var err error
	if modelUUser.Phone != "" || modelUUser.Password != "" {
		check = true
	} else {
		check = false
	}
	if check == true {
		fmt.Println("Secure update....")
		checkReg, phone, err := repo.CheckRegistrationToken(modelUUser.Token)
		if checkReg != true {
			return nil, 1, nil
		}
		if err != nil {
			return nil, 2, utility.ErrorHandler(errorPQX+": UpdateUserData.CheckRegistrationToken ", err)
		}
		repo.DeleteRegistrationToken(phone)
		fmt.Println("phone ", phone)
		idUser, err = repo.TakeIDUser(phone)
		fmt.Println("user ", idUser)
		if err != nil {
			return nil, 2, utility.ErrorHandler(errorPQX+": UpdateUserData.TakeIDUser ", err)
		}
	} else {
		fmt.Println(modelUUser.Token)
		checkReg, idUser, err = repo.CheckToken(modelUUser.Token)
		if checkReg != true {
			return nil, 1, nil
		}
		if err != nil {
			return nil, 2, utility.ErrorHandler(errorPQX+": UpdateUserData.CheckRegistrationToken ", err)
		}
	}
	err = repo.UpdateUserInDatabase(idUser, modelUUser)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorPQX+": UpdateUserData.UpdateUserInDatabase ", err)
	}
	repo.DeleteToken("", idUser)
	data, err := repo.SessionData(idUser, fbToken)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorPQX+": UpdateUserData.TakeToken ", err)
	}
	return data, 0, nil
}

//UpdateCarData - update car data in database
func UpdateCarData(repo ClientRepo, ctx *http.Request, modelUCar model.SetCarData, fbToken string) ([]byte, int, error) {
	if len(modelUCar.Token) < 120 {
		fmt.Println("Error token...")
		return nil, 1, nil
	}
	fmt.Println("Free update....")
	checkReg, idUser, err := repo.CheckToken(modelUCar.Token)
	if checkReg != true {
		return nil, 1, nil
	}
	if err != nil {
		return nil, 2, utility.ErrorHandler(errorPQX+": UpdateUserData.CheckRegistrationToken ", err)
	}
	_, err = repo.UpdateCarInDatabase(idUser, modelUCar)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorPQX+": UpdateUserData.UpdateUserInDatabase ", err)
	}
	data, err := repo.SessionData(idUser, fbToken)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorPQX+": UpdateUserData.TakeToken ", err)
	}
	return data, 0, nil
}

//RecoveryPass - recovery password user
func RecoveryPass(repo ClientRepo, token, pass string) (bool, error) {
	fmt.Println("Secure update....")
	check, phone, err := repo.CheckRegistrationToken(token)
	if check != true {
		return false, nil
	}
	if err != nil {
		return false, utility.ErrorHandler(errorPQX+": UpdateUserData.CheckRegistrationToken ", err)
	}
	repo.DeleteRegistrationToken(phone)
	err = repo.RecoveryPassInDatabase(phone, pass)
	if err != nil {
		return false, utility.ErrorHandler(" RecoveryPass.RecoveryPassInDatabase ", err)
	}
	return true, nil
}

//RemoveImage - remove image user
func RemoveImage(repo ClientRepo, token string) bool {
	check, idUser, err := repo.CheckToken(token)
	if err != nil || check != true {
		return false
	}
	err = repo.RemoveImageUserInDatabase(idUser)
	if err != nil {
		return false
	}
	return true
}

//SetImage - function if user set image
func SetImage(repo ClientRepo, token string) ([]byte, bool) {
	var phone model.One
	check, idUser, err := repo.CheckToken(token)
	if err != nil || check != true {
		check, phone.Value, err = repo.CheckRegistrationToken(token)
		fmt.Println("phone ", phone.Value)
		if err != nil || check != true {
			return nil, false
		}
	}
	if len(idUser) < 3 && len(phone.Value) == 10 {
		idUser, err = repo.TakeIDUser(phone.Value)
		if err != nil {
			return nil, false
		}
	}
	phone.Value, err = repo.SetImageUserInDatabase(idUser)
	if err != nil {
		return nil, false
	}
	data, err := json.Marshal(phone)
	if err != nil {
		fmt.Println(err.Error())
		return nil, false
	}
	return data, true
}

//DelCar -
func DelCar(repo ClientRepo, token string) (int, bool) {
	check, idUser, err := repo.CheckToken(token)
	if err != nil {
		return 1, false
	}
	if check != true {
		return 1, false
	}
	idCar, err := repo.TakeIDCar(idUser, true)
	if err != nil {
		return 0, false
	}
	res := repo.DeleteCarInDatabase(idCar)
	return 0, res
}

//SwitchStatusDriverOnUser
func SwitchStatusDriverOnUser(repo ClientRepo, token string) (int, error) {
	check, idUser, err := repo.CheckToken(token)
	if err != nil {
		return 0, err
	}
	if check != true {
		return 1, err
	}
	err = repo.SwitchModeDriver(idUser)
	return 0, err
}

//ListCabinetInCity - Список партнеров в городе.
func ListCabinetInCity(repo ClientRepo, token string) ([]byte, error) {
	_, idUser, err := repo.CheckToken(token)
	if err != nil {
		return nil, err
	}
	listCabinets, err := repo.GetListCabinetInCity(idUser)
	if err != nil {
		return nil, err
	}
	data, err := json.Marshal(listCabinets)
	if err != nil {
		return nil, err
	}
	return data, nil
}

//CheckRendAuto - Список предложенных автомобилей.
func CheckRendAuto(repo ClientRepo, token string) ([]byte, error) {
	_, idUser, err := repo.CheckToken(token)
	if err != nil {
		return nil, err
	}
	listAuto, err := repo.GetListCheckRendAuto(idUser)
	if err != nil {
		fmt.Println("empty list")
		return nil, err
	}
	if len(listAuto) == 0 || listAuto == nil {
		return nil, nil
	}
	data, err := json.Marshal(listAuto)
	if err != nil {
		return nil, err
	}
	return data, nil
}

//KeepAuto - Взять автомобиль в аренду
func KeepAuto(repo ClientRepo, token, idCar, idCabinet string) ([]byte, bool, error) {
	_, idUser, err := repo.CheckToken(token)
	if err != nil {
		return nil, false, err
	}
	check := repo.KeepAutoInDatabase(idUser, idCar, idCabinet)
	if !check {
		return nil, false, nil
	}
	data, err := repo.SessionData(idUser, "")
	if err != nil {
		return nil, false, err
	}
	return data, true, nil
}

//KeepOutAuto - Вернуть арендованный автомобиль
func KeepOutAuto(repo ClientRepo, token string) ([]byte, bool, error) {
	_, idUser, err := repo.CheckToken(token)
	if err != nil {
		return nil, false, err
	}
	check := repo.KeepOutAutoInDatabase(idUser)
	if !check {
		return nil, false, nil
	}
	data, err := repo.SessionData(idUser, "")
	if err != nil {
		return nil, false, err
	}
	return data, true, nil
}

//SessionsDriver
func SessionsDriver(repo ClientRepo, token string) ([]byte, error) {
	_, idUser, err := repo.CheckToken(token)
	if err != nil {
		return nil, err
	}
	idCity, err := repo.TakeIDCityUser(idUser)
	if err != nil {
		return nil, err
	}
	sessions, err := repo.GetSessionForDriver(idCity)
	data, err := json.Marshal(sessions)
	if err != nil {
		return nil, err
	}
	return data, nil
}

//YandexPay - send pay
func YandexPay(repo ClientRepo, ctx *http.Request) ([]byte, int, error) {
	var data []byte
	ctx.Body.Read(data)
	payData := model.Payment{}
	err := json.NewDecoder(ctx.Body).Decode(&payData)
	if err != nil {
		return nil, 2, err
	}
	//chenge created pay or create if not exist
	check, idUser, err := repo.CheckToken(payData.Token)
	if !check {
		return nil, 1, nil
	}
	if err != nil {
		return nil, 2, err
	}
	data, err = repo.SendPay(idUser, payData.Payment, payData.TypePay)
	if err != nil {
		return nil, 2, err
	}
	return data, 0, nil
}

//CreatePay - registration pay in server for yandex.kasssa
func CreatePay(repo ClientRepo, ctx *http.Request) ([]byte, error) {
	var data []byte
	ctx.Body.Read(data)
	payData := model.RegistrationPay{}
	err := json.NewDecoder(ctx.Body).Decode(&payData)
	//registration pay
	_, idUser, err := repo.CheckToken(payData.Token)
	if err != nil {
		return nil, err
	}
	data, err = repo.CreatePayInDatabase(payData, idUser)
	if err != nil {
		return nil, err
	}
	return data, err
}

//StatusPay - registration pay in server for yandex.kasssa
func StatusPay(repo ClientRepo, ctx *http.Request) error {
	var data []byte
	ctx.Body.Read(data)
	fmt.Println("Yandex.Event")
	// repo.ChangePayStatus()
	return nil
}

//SessionInformation -
func SessionInformation(repo ClientRepo, token string) ([]byte, error) {
	var data []byte
	_, idUser, err := repo.CheckToken(token)
	if err != nil {
		return nil, err
	}
	data, err = repo.SessionInformationFromDatabase(idUser)
	if err != nil {
		return nil, err
	}
	return data, nil
}
