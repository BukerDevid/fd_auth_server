package database

/*
Packet database for client API.
Author: GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru
*/

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	"../../utility"
	"../model"
)

const URLFULLDRIVER = "https://fulldriver.su:4777"

//CheckAuthUser - return of auth check{0-good,1-autorithation error,2-block user}
func (db *Database) CheckAuthUser(phone, pwd string) (string, int, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idUser, scanPass string
	var block bool
	_ = dbConn.QueryRow(context.Background(), "SELECT id, pwd, block_user FROM users WHERE phone = $1", phone).Scan(&idUser, &scanPass, &block)
	if block == true {
		return "", 2, nil
	}
	if idUser == "" || scanPass == "" {
		return "", 1, nil
	}
	if pwd != scanPass {
		return "", 1, nil
	}
	return idUser, 0, nil
}

//SessionData - load session data
func (db *Database) SessionData(idUser, fbToken string) ([]byte, error) {
	sessionData := model.SessionData{}
	generalData := model.GeneralData{}
	driverData := model.DriverData{}
	var err error
	//load user data
	generalData, sessionData.LastSession, err = db.loadUserDataForSession(idUser)
	if err != nil {
		return nil, err
	}
	//loda driver data
	driverData, err = db.loadDriverDataForSession(idUser)
	if err != nil {
		return nil, err
	}
	sessionData.Token, err = db.TakeToken(idUser, fbToken)
	sessionData.General = generalData
	sessionData.Driver = driverData
	db.checkOrder(idUser, fbToken)
	data, err := json.Marshal(sessionData)
	if err != nil {
		return nil, err
	}
	return data, err
}

func (db *Database) checkOrder(idUser, fbToken string) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idOrder string
	fmt.Println("")
	if err := dbConn.QueryRow(context.Background(), "SELECT id FROM trip_client WHERE client = $1 OR driver = $1 AND status_trip != 'end'", idUser).Scan(&idOrder); err != nil {
		return
	}
	fmt.Println("Data write notify - ", "load_order", idOrder, idUser, fbToken)
	dbConn.Exec(context.Background(), "SELECT pg_notify($1,$2)", "service", fmt.Sprintf("%v*%v*%v*%v", "load_order", idOrder, idUser, fbToken))
}

func (db *Database) loadUserDataForSession(idUser string) (model.GeneralData, bool, error) {
	generalData := model.GeneralData{}
	cityData := model.CityData{}
	ordersData := make([]model.OrderClientData, 0)
	var lastSession, image bool
	var idCity, idDriver, carDriver, rentDriver string
	var placesBinaryData []byte
	//Keep connect to database
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.QueryRow(context.Background(), "SELECT phone, username, city, rating, date_bir, sex, image_status, driver FROM users WHERE id = $1", idUser).Scan(&generalData.Phone, &generalData.UserName, &idCity, &generalData.Rating, &generalData.DateB, &generalData.Sex, &image, &lastSession)
	// fmt.Println(generalData.Phone, generalData.UserName, idCity, generalData.Rating, generalData.DateB, generalData.Sex, image, lastSession)
	//image
	if image == true {
		generalData.ImageStatus = fmt.Sprintf("%v/media/usr?phone=%v&token=%v", URLFULLDRIVER, generalData.Phone, takeMD5Sol(fmt.Sprintf("%v", time.Now().Unix())))
	} else {
		generalData.ImageStatus = ""
	}
	//load city
	dbConn.QueryRow(context.Background(), "SELECT reg.region || ' ' ||  city.city AS full_name, city.city, city.en_city, city.city_lat, city.city_lon, city.min_price FROM cities AS city, regions AS reg WHERE reg.id = city.region AND city.id = $1",
		idCity).Scan(&cityData.NameCity, &cityData.NameCitySh, &cityData.NameCityEn, &cityData.Lat, &cityData.Lon, &cityData.Price)
	//load orders
	resQuery, _ := dbConn.Query(context.Background(),
		"SELECT id, status_trip, in_city, places, price, babych, smoking, fastup, comment_exist, comment, animal, plane, date_plane, pay, driver FROM (SELECT * FROM trip_client WHERE status_trip != 'end' OR status_trip != 'end_trip' AND client = $1) AS trip WHERE status_trip = 'plan' OR trip.status_trip = 'plan_add_driver' OR trip.status_trip = 'plan_del_driver'",
		idUser)
	for resQuery.Next() {
		orderData := model.OrderClientData{}
		placesData := []model.PlaceData{}
		driverData := model.DriverOrder{}
		resQuery.Scan(&orderData.ID, &orderData.Status, &orderData.InCity, &placesBinaryData, &orderData.AddOption.Price, &orderData.AddOption.Babych,
			&orderData.AddOption.Smoking, &orderData.AddOption.FastUp, &orderData.AddOption.Comment, &orderData.AddOption.Animal, &orderData.AddOption.PlanOrder,
			&orderData.AddOption.PlanTime, &orderData.AddOption.SystemPay, &idDriver)
		if placesBinaryData != nil && len(placesBinaryData) > 0 {
			json.Unmarshal(placesBinaryData, &placesData)
		}
		dbConn.QueryRow(context.Background(), "SELECT car, car_arend FROM drivers WHERE id = $1", idUser).Scan(&carDriver, &rentDriver)
		if rentDriver != "" {
			dbConn.QueryRow(context.Background(), "SELECT us.username, us.phone, us.img, mk.mark, col.color, col.code, car.num_reg, owner.id, owner.title_ps FROM cars AS car, cabinet_ps AS owner, users AS us, color_auto AS col, mark AS mk WHERE mk.id = car.mark AND col.id = car.color AND car.id = $1 AND owner.id = owner_cabinet AND us.id = $2",
				rentDriver, idUser).Scan(&driverData.Name, &driverData.Phone, &image, &driverData.Car.Mark, &driverData.Car.Color.Title, &driverData.Car.Color.Value, &driverData.Car.Number, &driverData.Partner.ID, &driverData.Partner.Name)
			if image == true {
				driverData.Img = fmt.Sprintf("%v/media/usr?phone=%v&token=%v", URLFULLDRIVER, driverData.Phone, takeMD5Sol(fmt.Sprintf("%v", time.Now().Unix())))
			} else {
				driverData.Img = ""
			}
		} else if carDriver != "" {
			dbConn.QueryRow(context.Background(), "SELECT us.username, us.phone, us.img, mk.mark, col.color, col.code, car.num_reg FROM cars AS car, users AS us, color_auto AS col, mark AS mk WHERE mk.id = car.mark AND col.id = car.color AND car.id = $1 AND us.id = $2;",
				rentDriver, idUser).Scan(&driverData.Name, &driverData.Phone, &image, &driverData.Car.Mark, &driverData.Car.Color.Title, &driverData.Car.Color.Value, &driverData.Car.Number)
			if image == true {
				driverData.Img = fmt.Sprintf("%v/media/usr?phone=%v&token=%v", URLFULLDRIVER, driverData.Phone, takeMD5Sol(fmt.Sprintf("%v", time.Now().Unix())))
			} else {
				driverData.Img = ""
			}
		} else {
			//No driver
			orderData.Driver = driverData
		}
		if orderData.ID != "" {
			ordersData = append(ordersData, orderData)
		}
	}
	generalData.City = cityData
	generalData.Orders = ordersData
	return generalData, lastSession, nil
}

func (db *Database) loadDriverDataForSession(idUser string) (model.DriverData, error) {
	driverData := model.DriverData{}
	carData := model.CarData{}
	rentData := model.RentData{}
	ordersData := make([]model.OrderDriver, 0)
	var idCar, idRent string
	var placesBinaryData []byte
	//Keep connect to database
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	//load data
	if err := dbConn.QueryRow(context.Background(),
		"SELECT us.username, us.surname, us.middlename, dr.verification_driver, dr.car, dr.car_arend FROM users AS us, drivers AS dr WHERE dr.id = us.id AND us.id = $1",
		idUser).Scan(&driverData.Name, &driverData.Surname, &driverData.Middlename, &driverData.Verification, &idCar, &idRent); err != nil {
		driverData.Exist = false
	} else {
		driverData.Exist = true
	}
	//load car
	if idRent != "" {
		dbConn.QueryRow(context.Background(),
			"SELECT pr.id, pr.title_ps, col.color, col.code, mk.mark, car.num_reg, car.verification FROM cars AS car, cabinet_ps AS pr, color_auto AS col, mark AS mk WHERE col.id = car.color AND mk.id = car.mark AND car.id = $1 AND pr.id = car.owner_cabinet",
			idRent).Scan(&rentData.IDCabinet, &rentData.NameCabinet, &rentData.Car.Color.Title, &rentData.Car.Color.Value, &rentData.Car.Mark, &rentData.Car.NumReg, &rentData.Car.Verification)
		rentData.Car.Exist = true
		driverData.Rent = rentData
	}
	if idCar != "" {
		dbConn.QueryRow(context.Background(),
			"SELECT  col.color, col.code, mk.mark, car.num_reg, car.verification FROM cars AS car, color_auto AS col, mark AS mk WHERE col.id = car.color AND mk.id = car.mark AND car.id = $1",
			idCar).Scan(&carData.Color.Title, &carData.Color.Value, &carData.Mark, &carData.NumReg, &carData.Verification)
		carData.Exist = true
		driverData.Car = carData
	}
	//load orders
	resQuery, _ := dbConn.Query(context.Background(),
		"SELECT id, status_trip, in_city, places, price, babych, smoking, fastup, comment_exist, comment, animal, plane, date_plane, pay FROM (SELECT * FROM trip_client WHERE status_trip != 'end' OR status_trip != 'end_trip' AND driver = $1) AS trip WHERE status_trip = 'plan' OR trip.status_trip = 'plan_add_driver' OR trip.status_trip = 'plan_del_driver'",
		idUser)
	for resQuery.Next() {
		orderData := model.OrderDriver{}
		placesData := []model.PlaceData{}
		resQuery.Scan(&orderData.ID, &orderData.Status, &orderData.InCity, &placesBinaryData, &orderData.AddOption.Price, &orderData.AddOption.Babych,
			&orderData.AddOption.Smoking, &orderData.AddOption.FastUp, &orderData.AddOption.Comment, &orderData.AddOption.Animal, &orderData.AddOption.PlanOrder,
			&orderData.AddOption.PlanTime, &orderData.AddOption.SystemPay)
		if placesBinaryData != nil && len(placesBinaryData) > 0 {
			json.Unmarshal(placesBinaryData, &placesData)
		}
		if orderData.ID != "" {
			ordersData = append(ordersData, orderData)
		}
	}
	driverData.Orders = ordersData
	return driverData, nil
}

/*GetStartupData - return data:
 */
func (db *Database) GetStartupData() ([]byte, error) {
	return nil, nil
}

/*CheckUserInDatabase - return boolea READY*/
func (db *Database) CheckUserInDatabase(phone string) bool {
	var scanPhone string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return false
	}
	err = dbConn.QueryRow(context.Background(), "SELECT phone FROM users WHERE phone = $1", phone).Scan(&scanPhone)
	if err != nil {
		return false
	}
	if phone == scanPhone {
		return true
	}
	return false
}

/*GetSms - save key for sms*/
func (db *Database) GetSms(phone string) (string, error) {
	var actual int
	fmt.Println("Call sms", phone)
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	for {
		actual = rand.Intn(9999)
		if actual > 1000 {
			break
		}
	}
	key := fmt.Sprintf("%v", actual)
	dbConn.Exec(context.Background(), "DELETE FROM rtokens WHERE id = $1", phone)
	dbConn.Exec(context.Background(), "INSERT INTO rtokens(id, code_verification, date_of_code) VALUES($1,$2,$3)", phone, key, time.Now().Unix())
	return key, nil
}

/*CheckSmsKey - check sms intentification user*/
func (db *Database) CheckSmsKey(phone string, key string) (bool, error) {
	var readKey string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return false, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	err = dbConn.QueryRow(context.Background(), "SELECT code_verification FROM rtokens WHERE id = $1", phone).Scan(&readKey)
	if err != nil {
		return false, utility.ErrorHandler(errorPQX+"ChseckAuth.Query.Auth", err)
	}
	if key != readKey {
		return false, nil
	}
	return true, nil
}

/*TakeSmsForCheck - take sms for cheking of developer*/
func (db *Database) TakeSmsForCheck(phone string) ([]byte, error) {
	var readKey string
	modelSms := model.Sms{}
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	err = dbConn.QueryRow(context.Background(), "SELECT code_verification FROM rtokens WHERE id = $1", phone).Scan(&readKey)
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+"ChseckAuth.Query.Auth", err)
	}
	modelSms.Code = readKey
	data, err := json.Marshal(modelSms)
	return data, nil
}

/*LoadListCities - load list of active cities for client*/
func (db *Database) LoadListCities() ([]byte, error) {
	citiesJSON := model.Citis{}
	modelCity := model.CityData{}
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	ExQue, err := dbConn.Query(context.Background(), "SELECT regions.region || ' ' ||  cities.city AS res, cities.en_city, cities.city, cities.city_lat, cities.city_lon, cities.min_price FROM regions, cities WHERE regions.id = cities.region")
	for ExQue.Next() {
		ExQue.Scan(&modelCity.NameCity, &modelCity.NameCityEn, &modelCity.NameCitySh, &modelCity.Lat, &modelCity.Lon, &modelCity.Price)
		citiesJSON.City = append(citiesJSON.City, modelCity)
	}
	data, err := json.Marshal(citiesJSON)
	if err != nil {
		return nil, utility.ErrorHandler(errorJSON+".LoadListCities.Marshal", err)
	}
	return data, nil
}

/*CityNameByEnCityName - load name city form database where en name equals sityEn*/
func (db *Database) CityNameByEnCityName(cityEn string) (bool, string, error) {
	if len(cityEn) == 0 {
		return false, "", nil
	}
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return false, "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	var city string
	dbConn.QueryRow(context.Background(), "SELECT city FROM cities WHERE en_city = $1", cityEn).Scan(&city)
	fmt.Println("scan.city: ", city)
	if len(city) == 0 {
		return false, city, nil
	}
	return true, city, nil
}

/*LoadListAllCities - load list of all cities*/
func (db *Database) LoadListAllCities() ([]byte, error) {
	citiesJSON := model.Citis{}
	modelCity := model.CityData{}
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	ExQue, err := dbConn.Query(context.Background(), "SELECT regions.region || ' ' ||  res.city, res.en_city, res.city, res.city_lat, res.city_lon, res.min_price FROM regions, (SELECT city, region, en_city, city_lat, city_lon, min_price FROM cities WHERE active = true) AS res WHERE regions.id = res.region")
	for ExQue.Next() {
		ExQue.Scan(&modelCity.NameCity, &modelCity.NameCityEn, &modelCity.NameCitySh, &modelCity.Lat, &modelCity.Lon, &modelCity.Price)
		citiesJSON.City = append(citiesJSON.City, modelCity)
	}
	data, err := json.Marshal(citiesJSON)
	if err != nil {
		return nil, utility.ErrorHandler(errorJSON+".LoadListCities.Marshal", err)
	}
	return data, nil
}

/*LoadStart - load list of active cities for client*/
func (db *Database) LoadStart() ([]byte, error) {
	var loadStart model.Start
	citiesJSON := make([]model.Start, 0)
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	ExQue, err := dbConn.Query(context.Background(), "SELECT title, string, img FROM start_data")
	for ExQue.Next() {
		ExQue.Scan(&loadStart.Title, &loadStart.Value, &loadStart.Image)
		citiesJSON = append(citiesJSON, loadStart)
	}
	data, err := json.Marshal(citiesJSON)
	if err != nil {
		return nil, utility.ErrorHandler(errorJSON+".LoadListCities.Marshal", err)
	}
	return data, nil
}

//TakeIDCity - take id city from database
func (db *Database) TakeIDCity(city string) (string, error) {
	var idCity string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT id FROM (SELECT cities.id, regions.region || ' ' ||  cities.city AS res FROM regions, cities WHERE regions.id = cities.region) AS res WHERE res = $1",
		city).Scan(&idCity); err != nil {
		return "", utility.ErrorHandler(errorPQX+": TakeIDCity! ", err)
	}
	return idCity, nil
}

//LoadListColor - load list of color for client
func (db *Database) LoadListColor() ([]byte, error) {
	var loadColor model.Properties
	colorsJSON := make([]model.Properties, 0)
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	ExQue, err := dbConn.Query(context.Background(), "SELECT id, color, code FROM color_auto")
	for ExQue.Next() {
		ExQue.Scan(&loadColor.ID, &loadColor.Value, &loadColor.Code)
		colorsJSON = append(colorsJSON, loadColor)
	}
	data, err := json.Marshal(colorsJSON)
	if err != nil {
		return nil, utility.ErrorHandler(errorJSON+".LoadListColor.Marshal", err)
	}
	return data, nil
}

//TakeIDColor - take id color from database
func (db *Database) TakeIDColor(color string) (string, error) {
	var idColor string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT id FROM color_auto WHERE color = $1",
		color).Scan(&idColor); err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return idColor, nil
}

//LoadListMark - load list of mark for client
func (db *Database) LoadListMark() ([]byte, error) {
	var loadMark model.Properties
	markJSON := make([]model.Properties, 0)
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	ExQue, err := dbConn.Query(context.Background(), "SELECT id, mark FROM mark")
	for ExQue.Next() {
		ExQue.Scan(&loadMark.ID, &loadMark.Value)
		markJSON = append(markJSON, loadMark)
	}
	data, err := json.Marshal(markJSON)
	if err != nil {
		return nil, utility.ErrorHandler(errorJSON+".LoadListMark.Marshal", err)
	}
	return data, nil
}

//TakeIDMark - take id mark from database
func (db *Database) TakeIDMark(mark string) (string, error) {
	var idMark string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT id FROM mark WHERE mark = $1",
		mark).Scan(&idMark); err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return idMark, nil
}

//TakeIDUser -
func (db *Database) TakeIDUser(phone string) (string, error) {
	var idUser string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT id FROM users WHERE phone = $1",
		phone).Scan(&idUser); err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return idUser, nil
}

//RegistrationUserInDatabase - registration user in database
func (db *Database) RegistrationUserInDatabase(userData model.SetUserData) (string, error) {
	var idUser string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	idCity, err := db.TakeIDCity(userData.City)
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": RegistrationUserInDatabase.TakeIDCity! ", err)
	}
	fmt.Println("id Cities - " + idCity)
	if err = dbConn.QueryRow(context.Background(),
		"SELECT f_new_user($1,$2,$3,$4,$5,$6,$7,$8)", // 1.- Имя пользователя, 2.- Телефон, 3.- Пароль, 4.- Город, 5.- Дата регистрации, 6.- Дата рождения, 7.- Пол, 8.- Присутствие картинки
		userData.UserName,
		userData.Phone,
		userData.Password,
		idCity,
		time.Now().Unix(),
		userData.DateB,
		userData.Sex,
		userData.ImageStatus).Scan(&idUser); err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! Reg user ", err)
	}
	return idUser, nil
}

//UpdateUserInDatabase - secure update user data in database
func (db *Database) UpdateUserInDatabase(idUser string, modelUUser model.UpdateUserData) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	idCity, err := db.TakeIDCity(modelUUser.City)
	if err != nil {
		idCity = ""
	} else {
		fmt.Println("City id - ", idCity)
	}
	var id string
	dbConn.QueryRow(context.Background(),
		"SELECT f_update_user_secure($1,$2,$3,$4,$5,$6,$7,$8,$9)", // 1.- id пользователя,  2.- Имя пользователя, 3.- Телефон, 4.- Пароль
		idUser,
		modelUUser.Phone,
		modelUUser.Password,
		modelUUser.Username,
		modelUUser.Secondname,
		modelUUser.Middlname,
		idCity,
		modelUUser.DayB,
		modelUUser.Sex).Scan(&id)
	return nil
}

//DeleteUserInDatabase - delete user in database
func (db *Database) DeleteUserInDatabase(idUser string) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	dbConn.QueryRow(context.Background(),
		"SELECT f_del_user($1,$2)", idUser, time.Now().Unix()) // 1.- id пльзователя, 2.- Время удаления
	return nil
}

//BlockUserInDatabase - delete user in database
func (db *Database) BlockUserInDatabase(idUser string) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	dbConn.QueryRow(context.Background(),
		"SELECT f_block_user($1,$2)", idUser, time.Now().Unix()) // 1.- id пльзователя, 2.- Время удаления
	return nil
}

//RecoveryUserInDatabase - recovery user in database
func (db *Database) RecoveryUserInDatabase(idUser string) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	dbConn.QueryRow(context.Background(),
		"SELECT f_rec_user($1,$2)", idUser, time.Now().Unix()) // 1.- id пльзователя, 2.- Время восстановления
	return nil
}

//UBlockUserInDatabase - recovery user in database
func (db *Database) UBlockUserInDatabase(idUser string) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	dbConn.QueryRow(context.Background(),
		"SELECT f_ublock_user($1,$2)", idUser, time.Now().Unix()) // 1.- id пльзователя, 2.- Время восстановления
	return nil
}

//RegistrationDriverInDatabase - registratino driver in database
func (db *Database) RegistrationDriverInDatabase(driverData model.SetDriverData, idUser string) (string, int, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", 0, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(),
		"SELECT f_new_driver($1,$2,$3,$4,$5,$6)", //1- id пользователя, 2.- Имя, 3.- Фамилия, 4.- Отчество, 5.- Серия, 6.- Номер удостоверения, 7.- Статус изображения
		idUser,
		driverData.Name,
		driverData.Surname,
		driverData.Middlename,
		driverData.Serial,
		driverData.Number).Scan(&idUser); err != nil {
		return "", 1, nil
	}
	if idUser == "" {
		return "", 1, nil
	}
	return idUser, 0, nil
}

//UpdateDriverInDatabase - updating driver data in database
func (db *Database) UpdateDriverInDatabase(idUser string, driverData model.SetDriverData) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	dbConn.QueryRow(context.Background(),
		"SELECT f_update_driver($1,$2,$3)", //1- id пользователя, 2.- Фамилия, 3.- Отчество
		idUser,
		driverData.Surname,
		driverData.Middlename)
	return nil
}

//RemoveImageUserInDatabase - remove user image in database
func (db *Database) RemoveImageUserInDatabase(idUser string) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	_, err = dbConn.Exec(context.Background(), "UPDATE users SET image_status = FALSE WHERE id = $1", idUser)
	if err != nil {
		return utility.ErrorHandler(" RecoveryPassInDatabase.Exec ", err)
	}
	return nil
}

//SetImageUserInDatabase - update status image in database on true
func (db *Database) SetImageUserInDatabase(idUser string) (string, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	var phone string
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	err = dbConn.QueryRow(context.Background(), "UPDATE users SET image_status = TRUE WHERE id = $1 RETURNING phone", idUser).Scan(&phone)
	if err != nil {
		return "", utility.ErrorHandler(" RecoveryPassInDatabase.Exec ", err)
	}
	return phone, nil
}

//RecoveryPassInDatabase - recovery passwprd user
func (db *Database) RecoveryPassInDatabase(phone, pass string) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	_, err = dbConn.Exec(context.Background(), "UPDATE users SET pwd = $1 WHERE phone = $2", pass, phone)
	if err != nil {
		return utility.ErrorHandler(" RecoveryPassInDatabase.Exec ", err)
	}
	return nil
}

//RegistrationCarInDatabase - registration car in daabase
func (db *Database) RegistrationCarInDatabase(carData model.SetCarData, idUser, fbToken string) ([]byte, int, error) { //0 - success, 1 - car exist, 2 - other error
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	var idCar, checkID string
	if err = dbConn.QueryRow(context.Background(),
		"SELECT f_new_car($1,$2,$3)", //
		carData.Mark,
		carData.Color,
		carData.NumReg).Scan(&idCar); err != nil {
		return nil, 1, utility.ErrorHandler(errorPQX+": ACQUIR! Reg car ", err)
	}
	fmt.Println("Id new car = ", idCar)
	if idCar == "" {
		return nil, 2, nil
	}
	checkID, check, err := db.KeepOwnerUserToCar(idCar, idUser)
	if check != 0 {
		return nil, check, nil
	}
	fmt.Println("id new owner - ", checkID)
	if checkID != idCar {
		return nil, 2, nil
	}
	fmt.Println("get auto data")
	data, err := db.SessionData(idUser, fbToken)
	if err != nil {
		return nil, 2, utility.ErrorHandler(" RegistrationCarInDatabase.GetCarData ", err)
	}
	fmt.Println("return auto data")
	return data, 0, nil
}

//UpdateCarInDatabase - updating car in database
func (db *Database) UpdateCarInDatabase(idUser string, CarData model.SetCarData) (string, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	idCar, err := db.TakeIDCarOnRegistrationNumber(CarData.NumReg)
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": UpdateCarInDatabase.TakeIDCar ", err)
	}
	fmt.Printf("\nupdate car data - new data %v %v %v\n", idCar, CarData.Color, CarData.Mark)
	if err := dbConn.QueryRow(context.Background(),
		"SELECT f_update_car($1,$2,$3)", //1- id пользователя
		idCar,
		CarData.Color,
		CarData.Mark).Scan(&idCar); err != nil {
		return "", err
	}
	return idCar, nil
}

//CheckExist - check auto on exists
func (db *Database) CheckExist(registrationNumber string) (bool, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return true, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT id FROM cars WHERE num_reg = $1", registrationNumber).Scan(&registrationNumber); err != nil {
		return false, nil
	}
	return true, nil
}

//CheckOwnerCar - check auto on exists
func (db *Database) CheckOwnerCar(registrationNumber string) (bool, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return true, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(), "SELECT id FROM (SELECT * FROM cars WHERE num_reg = $1) as car WHERE car.owner_user != '' OR car.owner_cabinet != ''", registrationNumber).Scan(&registrationNumber); err != nil {
		fmt.Println("No owner")
		return false, nil
	}
	fmt.Println("Yes owner")
	return true, nil
}

//KeepOwnerUserToCar - keep user owner to car
func (db *Database) KeepOwnerUserToCar(idCar, idUser string) (string, int, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", 0, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	if err = dbConn.QueryRow(context.Background(),
		"SELECT f_keep_owner_user($1,$2)", //1- id пользователя, 2.- Имя, 3.- Фамилия, 4.- Отчество, 5.- Серия, 6.- Номер удостоверения, 7.- Статус изображения
		idCar,
		idUser).Scan(&idCar); err != nil {
		return "", 2, utility.ErrorHandler(errorPQX+": ACQUIR! keep owner ", err)
	}
	return idCar, 0, nil
}

//KeepCarInDatabase
func (db *Database) KeepCarInDatabase(idUser string, CarData model.SetCarData) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	return nil
}

//DelCarInDatabase
func (db *Database) DeleteCarInDatabase(idCar string) bool {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return false
	}
	fmt.Println("Delete car (Keep out)")
	dbConn.QueryRow(context.Background(), "SELECT f_del_owner($1)", idCar).Scan(&idCar)
	if idCar == "" {
		return false
	}
	return true
}

//KeepoutCarInDatabase
func (db *Database) KeepoutCarInDatabase(idUser string) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	dbConn.QueryRow(context.Background(), "SELECT f_del_owner($1)", idUser).Scan()
	return nil
}

//TakeIDCar
func (db *Database) TakeIDCar(idUser string, typeCar bool) (string, error) { // false - rend car|true - user car
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	var idCar string
	if typeCar == true {
		dbConn.QueryRow(context.Background(), "SELECT car FROM drivers WHERE id = $1", idUser).Scan(&idCar)
	} else {
		dbConn.QueryRow(context.Background(), "SELECT car_arend FROM drivers WHERE id = $1", idUser).Scan(&idCar)
	}
	return idCar, nil
}

//TakeIDCarOnRegistrationNumber
func (db *Database) TakeIDCarOnRegistrationNumber(registrationNumber string) (string, error) { // false - rend car|true - user car
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	var idCar string
	dbConn.QueryRow(context.Background(), "SELECT id FROM cars WHERE num_reg = $1", registrationNumber).Scan(&idCar)
	return idCar, nil
}

//SwitchModeDriver
func (db *Database) SwitchModeDriver(idUser string) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	dbConn.QueryRow(context.Background(), "SELECT f_switch_user_mode($1)", idUser)
	return nil
}

//GetListCabinetInCity
func (db *Database) GetListCabinetInCity(idUser string) ([]model.GetCabinetDataDriver, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	var idCity string
	var cabinets []model.GetCabinetDataDriver
	defer dbConn.Release()
	if err = dbConn.QueryRow(context.Background(), "SELECT city FROM users WHERE id = $1", idUser).Scan(&idCity); err != nil {
		return cabinets, err
	}
	fmt.Println(idCity)
	query, err := dbConn.Query(context.Background(), "SELECT id, title_ps, lat, lon, addr FROM cabinet_ps WHERE city = $1 AND del_cabinet != TRUE AND id LIKE '%PS%'", idCity)
	if err != nil {
		return cabinets, err
	}
	for query.Next() {
		var cabinet model.GetCabinetDataDriver
		if err = query.Scan(&cabinet.ID, &cabinet.Title, &cabinet.Lat, &cabinet.Lon, &cabinet.Addr); err != nil {
			return nil, err
		}
		fmt.Println(cabinet.ID, cabinet.Title, cabinet.Lat, cabinet.Lon)
		cabinets = append(cabinets, cabinet)
	}
	return cabinets, nil
}

//GetListCheckRendAuto
func (db *Database) GetListCheckRendAuto(idUser string) ([]model.GetCarDataFromCabinetDriver, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	var cars []model.GetCarDataFromCabinetDriver
	defer dbConn.Release()
	if err != nil {
		return nil, utility.ErrorHandler(errorPQX+": ACQUIR! ", err)
	}
	query, err := dbConn.Query(context.Background(), "SELECT cr.id, ca.color, ca.code, mk.mark, cr.num_reg, cb.id, cb.title_ps FROM cars_cabinet AS ccb, color_auto AS ca, mark AS mk, cars AS cr, cabinet_ps AS cb WHERE ccb.given_user = $1 AND cr.id = ccb.car AND cb.id = ccb.cabinet AND ca.id = cr.color AND mk.id = cr.mark", idUser)
	if err != nil {
		return nil, err
	}
	for query.Next() {
		var modelGetCarData model.GetCarDataFromCabinetDriver
		query.Scan(&modelGetCarData.ID, &modelGetCarData.Color.Title, &modelGetCarData.Color.Value, &modelGetCarData.Mark, &modelGetCarData.NumReg, &modelGetCarData.IDCabinet, &modelGetCarData.NameCabinet)
		cars = append(cars, modelGetCarData)
	}
	return cars, nil
}

//KeepAutoInDatabase
func (db *Database) KeepAutoInDatabase(idUser, idCar, idCabinet string) bool {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idCarScan string
	unix := time.Now().Unix()
	dbConn.QueryRow(context.Background(), "SELECT f_start_rent_car($1,$2,$3,$4)", idCar, idCabinet, idUser, unix).Scan(&idCarScan)
	if idCarScan == idCar {
		return true
	}
	return false
}

//KeepOutAutoInDatabase
func (db *Database) KeepOutAutoInDatabase(idUser string) bool {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idHistoryScan, idHistory string
	if err := dbConn.QueryRow(context.Background(), "SELECT rent_id FROM cars WHERE rent_user = $1", idUser).Scan(&idHistory); err != nil {
		return false
	}
	unix := time.Now().Unix()
	dbConn.QueryRow(context.Background(), "SELECT f_end_rent_car($1,$2)", idHistory, unix).Scan(&idHistoryScan)
	if idHistoryScan == idHistory {
		return true
	}
	return false
}

//TakeIDCityUser -
func (db *Database) TakeIDCityUser(idUser string) (string, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idCity string
	if err := dbConn.QueryRow(context.Background(), "SELECT city FROM users WHERE id = $1", idUser).Scan(&idCity); err != nil {
		return "", err
	}
	return idCity, nil
}
