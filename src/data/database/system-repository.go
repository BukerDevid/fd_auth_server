package database

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"../../utility"
	"../model"
)

//SystemRepo - it's interface for using admin agents and cabinets
type SystemRepo interface {
	CheckAuthAdmin(phone, pwd string) (string, int, int, error)
	SessionAdminData(idUser string, prin int) ([]byte, error)
	CheckAuthAgent(phone, pwd string) (string, int, error)
	SessionAgentData(idUser string) ([]byte, error)
	CheckTokenA(token string) (bool, string, error)
	CheckTokenP(token string) (bool, string, error)
	RegistrationAgentInDatabase(modelAgentData model.SetAgentData) error
	RegistrationCabinetPS(modelCabinetData model.SetCabinetDataData) (int, error)
	RegistrationCabinetFD(modelCabinetData model.SetCabinetDataData) (int, error)
	GetListUsers(all, driver, verification bool) ([]byte, error)
	GetListCars(all, verification bool) ([]byte, error)
	SetVerificationDriver(idUser string) error
	DeleteDriverDataUser(idUser string) error
	SetVerificationCar(idUser string) error
	DeleteDriverDataCar(idUser string) error
	TakeIDUser(phone string) (string, error)
}

//AuthAgent - Authorithation agent
func AuthAgent(repo SystemRepo, phone, pwd, fbToken string) ([]byte, int, error) {
	idUser, check, err := repo.CheckAuthAgent(phone, pwd)
	if err != nil {
		return nil, check, utility.ErrorHandler(errorRepo+": CheckAuth", err)
	}
	if check != 0 {
		return nil, check, nil
	}
	data, err := repo.SessionAgentData(idUser)
	return data, 0, err
}

//AuthAdmin - Authorithation admin
func AuthAdmin(repo SystemRepo, phone, pwd, fbToken string) ([]byte, int, error) {
	fmt.Println("Request from database")
	idUser, priv, check, err := repo.CheckAuthAdmin(phone, pwd)
	if err != nil {
		fmt.Println(utility.ErrorHandler(errorRepo+": CheckAuth", err).Error())
		return nil, check, utility.ErrorHandler(errorRepo+": CheckAuth", err)
	}
	if check != 0 {
		return nil, check, nil
	}
	data, err := repo.SessionAdminData(idUser, priv)
	return data, 0, err
}

//CheckTokenAdmin - check token for media server
func CheckTokenAdmin(repo SystemRepo, token string) (int, error) {
	fmt.Println("Request from database")
	check, _, err := repo.CheckTokenA(token)
	if err != nil {
		fmt.Println(utility.ErrorHandler(errorRepo+": CheckAuth", err).Error())
		return 1, utility.ErrorHandler(errorRepo+": CheckAuth", err)
	}
	if check != true {
		return 1, nil
	}
	return 0, err
}

//RegistrationAgent -
func RegistrationAgent(repo SystemRepo, ctx *http.Request) ([]byte, int, error) {
	var data []byte
	ctx.Body.Read(data)
	var modelAgentData model.SetAgentData
	err := json.NewDecoder(ctx.Body).Decode(&modelAgentData)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorJSON+": RegistrationDriver.NewDecoder", err)
	}
	check, _, err := repo.CheckTokenA(modelAgentData.Token)
	if check != true {
		return nil, 3, nil
	}
	err = repo.RegistrationAgentInDatabase(modelAgentData)
	if err != nil {
		return nil, 3, err
	}
	return data, 0, nil
}

//RegistrationCabinet -
func RegistrationCabinet(repo SystemRepo, ctx *http.Request) ([]byte, int, error) {
	var data []byte
	var result int
	var modelCabinetData model.SetCabinetDataData
	err := json.NewDecoder(ctx.Body).Decode(&modelCabinetData)
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorJSON+": RegistrationCabinet.RegistrationCabinet", err)
	}
	check, _, err := repo.CheckTokenA(modelCabinetData.Token)
	if check != true {
		return nil, 3, nil
	}
	if err != nil {
		return nil, 0, utility.ErrorHandler(errorJSON+": RegistrationCabinet.CheckToken", err)
	}
	if modelCabinetData.Type == "ps" {
		result, err = repo.RegistrationCabinetPS(modelCabinetData)
	} else if modelCabinetData.Type == "fd" {
		result, err = repo.RegistrationCabinetFD(modelCabinetData)
	}
	if err != nil {
		return nil, 3, nil
	}
	return data, result, nil
}

//CheckAdministratorToken - for check admin token
func CheckAdministratorToken(repo SystemRepo, token string) error {
	check, _, err := repo.CheckTokenA(token)
	if !check || err != nil {
		return errors.New("No auth user")
	} else {
		return nil
	}
}

//ListUserForSystem - list user for admin panel
func ListUserForSystem(repo SystemRepo, all, driver, verification bool) ([]byte, error) {
	return repo.GetListUsers(all, driver, verification)
}

//ListUserForSystem - list user for admin panel
func ListCarsForSystem(repo SystemRepo, all, verification bool) ([]byte, error) {
	return repo.GetListCars(all, verification)
}

//VerificationForDriver -
func VerificationForDriver(repo SystemRepo, phone string, verification bool) error {
	idUser, err := repo.TakeIDUser(phone)
	if err == nil {
		if verification {
			return repo.SetVerificationDriver(idUser)
		} else {
			return repo.DeleteDriverDataUser(idUser)
		}
	} else {
		return err
	}
}

//VerificationForCar -
func VerificationForCar(repo SystemRepo, phone string, verification bool) error {
	idUser, err := repo.TakeIDUser(phone)
	if err == nil {
		if verification {
			return repo.SetVerificationCar(idUser)
		} else {
			return repo.DeleteDriverDataCar(idUser)
		}
	} else {
		return err
	}
}
