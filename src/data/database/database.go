package database

/*
Packet database for client API.
Author: GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru
*/

import (
	"context"
	"crypto/md5"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"io"
	"math/rand"
	"time"

	"../../utility"
	pgx "github.com/jackc/pgx"
	pgxp "github.com/jackc/pgx/pgxpool"
)

var (
	errorPQX     = "Database driver PGX# "
	errorListen  = "Database modul Listen error# "
	errorActHand = "Database modul ActHandlers error# "
	errorJSON    = "Database modul JSON error# "
)

/*
Database is type for interaction with local database. She has methods initialization, opening connection and closing connection.
Fileds:
connectionStrPG string - config URI string for connection to PostgreSQL database
dbConnectPool   *pgx.ConnPool - keep pool connection to PostgreSQL database, for keep one connects use pool.Acquire() (conn, error)
*/
type Database struct {
	connectionStrPG string
	dbConnectPool   *pgxp.Pool
	dbConnectListen *pgx.Conn
}

//DBInstance - return of instance database
func DBInstance(connStrPG string) *Database {
	return &Database{connectionStrPG: connStrPG}
}

//TakeSha256Sol - Sol for token
func takeSha256Sol() string {
	actual := time.Now().Unix()
	actual = actual + rand.Int63n(33)
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(actual))
	hash := sha256.Sum256(b)
	return fmt.Sprintf("%x", hash)
}

//TakeMD5Sol - Sol for token
func takeMD5Sol(idUser string) string {
	hash := md5.New()
	io.WriteString(hash, fmt.Sprintf("%v,%v", "BigBrotherSeeYou", idUser))
	return fmt.Sprintf("%x", hash.Sum(nil))
}

//TakeSha256Tok - Tok
func takeSha256Tok(idUser string) string {
	b := make([]byte, 8)
	b = []byte(idUser)
	hash := sha256.Sum256(b)
	return fmt.Sprintf("%x", hash)
}

//TakeToken - taking a user token  ** Rewrite Query **
func (db *Database) TakeToken(idUser, fbToken string) (string, error) {
	db.DeleteToken("", idUser)
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	actualTime := time.Now().Unix()
	resultHash := fmt.Sprintf("%v_%v_%v", takeSha256Sol(), takeSha256Tok(idUser), takeMD5Sol(idUser))
	_, err = dbConn.Exec(context.Background(), "INSERT INTO tokens VALUES($1,$2,$3,$4,$5)", idUser, resultHash, actualTime, fbToken, actualTime)
	if err != nil {
		return resultHash, utility.ErrorHandler(errorPQX+".TakeToken.Exec", err)
	}
	return resultHash, nil
}

//TakeRegistrationToken - taking a user token ** Rewrite Query **
func (db *Database) TakeRegistrationToken(phone string) (string, error) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	actualTime := time.Now().Unix()
	resultHash := fmt.Sprintf("%v_%v_%v", takeSha256Sol(), takeSha256Tok(phone), takeMD5Sol(phone))
	_, err = dbConn.Exec(context.Background(), "UPDATE rtokens SET token = $1, date_of_token = $2 WHERE id = $3", resultHash, actualTime, phone)
	if err != nil {
		return "", utility.ErrorHandler(errorPQX+".TakeRegistrationToken.CallSms", err)
	}
	return resultHash, nil
}

//CheckToken - checking a user token on actuality
func (db *Database) CheckToken(token string) (bool, string, error) {
	var idUser string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	rowFunction := dbConn.QueryRow(context.Background(), "SELECT id FROM tokens WHERE token = $1", token)
	if err = rowFunction.Scan(&idUser); err != nil {
		return false, "", err
	}
	fmt.Println("Id user: ", idUser)
	return true, idUser, nil
}

/*CheckRegistrationToken  - return data:
- phone
*/
func (db *Database) CheckRegistrationToken(token string) (bool, string, error) {
	var scanPhone string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return false, "", err
	}
	rowFunction := dbConn.QueryRow(context.Background(), "SELECT id FROM rtokens WHERE token = $1", token)
	if err := rowFunction.Scan(&scanPhone); err != nil {
		return false, "", err
	}
	return true, scanPhone, nil
}

//TakeTokenA - taking a user token  ** Rewrite Query **
func (db *Database) TakeTokenA(idUser, fbToken string) (string, error) {
	db.DeleteTokenA("", idUser)
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	actualTime := time.Now().Unix()
	resultHash := fmt.Sprintf("%v_%v_%v", takeSha256Sol(), takeSha256Tok(idUser), takeMD5Sol(idUser))
	_, err = dbConn.Exec(context.Background(), "INSERT INTO atokens VALUES($1,$2,$3,$4,$5)", idUser, resultHash, actualTime, fbToken, actualTime)
	if err != nil {
		return resultHash, utility.ErrorHandler(errorPQX+".TakeToken.Exec", err)
	}
	return resultHash, nil
}

//CheckTokenA - checking a user token on actuality
func (db *Database) CheckTokenA(token string) (bool, string, error) {
	var idUser string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	rowFunction := dbConn.QueryRow(context.Background(), "SELECT id FROM atokens WHERE token = $1", token)
	if err = rowFunction.Scan(&idUser); err != nil {
		return false, "", err
	}
	fmt.Println("Id user: ", idUser)
	return true, idUser, nil
}

//TakeTokenP - taking a user token  ** Rewrite Query **
func (db *Database) TakeTokenP(idUser, fbToken string) (string, error) {
	db.DeleteToken("", idUser)
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	actualTime := time.Now().Unix()
	resultHash := fmt.Sprintf("%v_%v_%v", takeSha256Sol(), takeSha256Tok(idUser), takeMD5Sol(idUser))
	_, err = dbConn.Exec(context.Background(), "INSERT INTO ptokens VALUES($1,$2,$3,$4,$5)", idUser, resultHash, actualTime, fbToken, actualTime)
	if err != nil {
		return resultHash, utility.ErrorHandler(errorPQX+".TakeToken.Exec", err)
	}
	return resultHash, nil
}

//CheckTokenP - checking a user token on actuality
func (db *Database) CheckTokenP(token string) (bool, string, error) {
	var idUser string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	rowFunction := dbConn.QueryRow(context.Background(), "SELECT id FROM ptokens WHERE token = $1", token)
	if err = rowFunction.Scan(&idUser); err != nil {
		return false, "", err
	}
	fmt.Println("Id user: ", idUser)
	return true, idUser, nil
}

//DeleteToken - delete a user token
func (db *Database) DeleteToken(token, idUser string /*token or id*/) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+".DeleteToken.", err)
	}
	if token != "" {
		rowFunction := dbConn.QueryRow(context.Background(), "SELECT token FROM tokens WHERE token = $1", token)
		if err := rowFunction.Scan(&token); err != nil {
			return utility.ErrorHandler(errorPQX+".DeleteToken.QueryRow.TokenOnToken", err)
		}
		dbConn.Exec(context.Background(), "DELETE FROM tokens WHERE token = $1", token)
	} else {
		rowFunction := dbConn.QueryRow(context.Background(), "SELECT token FROM tokens WHERE id = $1", idUser)
		if err := rowFunction.Scan(&token); err != nil {
			return utility.ErrorHandler(errorPQX+".DeleteToken.QueryRow.TokenOnId", err)
		}
		dbConn.Exec(context.Background(), "DELETE FROM tokens WHERE id = $1", idUser)
	}
	return nil
}

//DeleteTokenA - delete a user token
func (db *Database) DeleteTokenA(token, idUser string /*token or id*/) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+".DeleteTokenA.", err)
	}
	if token != "" {
		rowFunction := dbConn.QueryRow(context.Background(), "SELECT atoken FROM tokens WHERE token = $1", token)
		if err := rowFunction.Scan(&token); err != nil {
			return utility.ErrorHandler(errorPQX+".DeleteTokenA.QueryRow.TokenOnToken", err)
		}
		dbConn.Exec(context.Background(), "DELETE FROM tokens WHERE token = $1", token)
	} else {
		rowFunction := dbConn.QueryRow(context.Background(), "SELECT atoken FROM tokens WHERE id = $1", idUser)
		if err := rowFunction.Scan(&token); err != nil {
			return utility.ErrorHandler(errorPQX+".DeleteTokenA.QueryRow.TokenOnId", err)
		}
		dbConn.Exec(context.Background(), "DELETE FROM atokens WHERE id = $1", idUser)
	}
	return nil
}

//DeleteRegistrationToken - delete a user token
func (db *Database) DeleteRegistrationToken(phone string /*phone*/) error {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return utility.ErrorHandler(errorPQX+".DeleteToken.", err)
	}
	dbConn.Exec(context.Background(), "DELETE FROM rtokens WHERE id = $1", phone)
	return nil
}

//InitDatabaseConnections - opening connection to database
func (db *Database) InitDatabaseConnections() error {
	confConn, err := pgxp.ParseConfig(db.connectionStrPG) // Example: user=jack password=secret host=host1,host2,host3 port=5432,5433,5434 dbname=mydb sslmode=verify-ca
	if err != nil {                                       // https://www.postgresql.org/docs/11/libpq-connect.html#LIBPQ-MULTIPLE-HOSTS
		return utility.ErrorHandler(errorPQX+" ParseURI ", err)
	}
	db.dbConnectPool, err = pgxp.ConnectConfig(context.Background(), confConn)
	if err != nil {
		return utility.ErrorHandler(errorPQX+" NewConnPool ", err)
	}
	return nil
}

//CheckCity - cheking city in database
func (db *Database) CheckCity(city string) bool {
	var enCity string
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		return false
	}
	dbConn.QueryRow(context.Background(), "SELECT en_city FROM cities WHERE en_city = $1", city).Scan(&enCity)
	if enCity != city {
		return false
	}
	return true
}
