package sms_server

import "fmt"

//SMS
type SMS struct {
	ServerOne *SmsReserver
	ServerTwo *SmsReserver
}

//AnswerPROSTOR
type AnswerPROSTOR struct {
	Status  string                 `json:"status"`
	Balance interface{}            `json:"balance"`
	Message []MessageAnswerPROSTOR `json:"messages"`
}

//MessageAnswerPROSTOR
type MessageAnswerPROSTOR struct {
	Status string `json:"status"`
	SMSid  string `json:"smscId"`
	ID     string `jsonL:"clientId"`
}

//SmsReserver
type SmsReserver struct {
	Login string
	Pwd   string
	OAuth string
	URL   string
}

//SmsWrite - struct write sms
type SmsWrite struct {
	Message []MessageData `json:"messages"`
	Login   string        `json:"login"`
	Pwd     string        `json:"password"`
}

type MessageData struct {
	Phone    string `json:"phone"`
	Sender   string `json:"sender"`
	IDClient string `json:"clientId"`
	Value    string `json:"text"`
}

func InitSmsReservers() *SMS {
	reservers := &SMS{
		ServerOne: initSmsReserverPROSTOR(),
		ServerTwo: initSmsReserverSMSRU(),
	}
	return reservers
}

//InitSmsReserver
func initSmsReserverPROSTOR() *SmsReserver {
	reserver := &SmsReserver{
		Login: "t89658335649",
		Pwd:   "709531",
		OAuth: "",
		URL:   "http://api.prostor-sms.ru/",
	}
	fmt.Println("init: ", reserver.Login, reserver.Pwd, reserver.URL)
	return reserver
}

//InitSmsReserver
func initSmsReserverSMSRU() *SmsReserver {
	reserver := &SmsReserver{
		Login: "",
		Pwd:   "",
		OAuth: "4A2A5A0C-1A02-5B19-275B-AA9D48EE5CBD",
		URL:   "https://sms.ru/",
	}
	return reserver
}
