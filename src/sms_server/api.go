package sms_server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

//WriteSms
func (sms *SMS) WriteSms(phone, key string) error {
	//First Sms reserver
	url := fmt.Sprintf("%v%v", sms.ServerOne.URL, "messages/v2/send.json")
	message := []MessageData{}
	message = append(message, MessageData{Phone: fmt.Sprintf("+7%v", phone), IDClient: "one", Value: key})
	data := SmsWrite{
		Message: message,
		Login:   sms.ServerOne.Login,
		Pwd:     sms.ServerOne.Pwd,
	}
	body, err := json.Marshal(data)
	if err != nil {
		return err
	}
	resp, err := http.Post(url, "application/json", bytes.NewReader(body))
	if err != nil {
		return err
	}
	if resp.StatusCode == 200 {
		readBody := MessageAnswerPROSTOR{}
		json.NewDecoder(resp.Body).Decode(&readBody)
		fmt.Println("sms: ", readBody.Status)
	} else if resp.StatusCode == 401 {
		fmt.Println("sms: No auth on sms reserver")
		sms.WriteSmsSecondServer(phone, key)
	} else {
		fmt.Println("sms: Unknow error")
		sms.WriteSmsSecondServer(phone, key)
	}
	return nil
}

//WriteSmsSecondServer
func (sms *SMS) WriteSmsSecondServer(phone, key string) error {
	url := fmt.Sprintf("%vsms/send?api_id=%v&to=%v&msg=%v", sms.ServerTwo.URL, sms.ServerTwo.OAuth, phone, key)
	fmt.Println(url)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	// var data []byte
	// // var out interface
	// resp.Body.Read(data)
	if resp.StatusCode == 200 {
		fmt.Println("sms: Succes write server 2")
	} else {
		fmt.Println("sms: error write server 2")
	}
	return nil
}
