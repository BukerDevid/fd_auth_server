package server

import (
	"fmt"
	"net/http"

	"../../data/database"
	"../../data/model"

	"github.com/gorilla/mux"
)

//Handlers - router for server
func (root *Core) Handlers(errCh chan<- string) {
	//For User or Client
	root.proc.router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		w.WriteHeader(200)
	}).Methods("GET")
	//Check server
	root.proc.router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		w.WriteHeader(200)
	}).Methods("OPTIONS")
	//Выгрузка стартовых данных.
	root.proc.router.HandleFunc("/auth/start", func(w http.ResponseWriter, r *http.Request) {
		data, err := database.ListStartData(root.db)
		writeStandartData(data, err, w, errCh)
	}).Methods("GET")
	//Выгрузка активных городов. (Для регистрации пользователей)
	root.proc.router.HandleFunc("/auth/cities", func(w http.ResponseWriter, r *http.Request) {
		data, err := database.ListCities(root.db)
		writeStandartData(data, err, w, errCh)
	}).Methods("GET")
	//List color
	root.proc.router.HandleFunc("/auth/colors", func(w http.ResponseWriter, r *http.Request) {
		data, err := database.ListColors(root.db)
		writeStandartData(data, err, w, errCh)
	}).Methods("GET")
	//List marks
	root.proc.router.HandleFunc("/auth/marks", func(w http.ResponseWriter, r *http.Request) {
		data, err := database.ListMarks(root.db)
		writeStandartData(data, err, w, errCh)
	}).Methods("GET")
	//Обновлени/восстановление данных.
	root.proc.router.HandleFunc("/auth/update/user", func(w http.ResponseWriter, r *http.Request) {
		UpdateOnlyPassword := false
		modelUUser := model.UpdateUserData{}
		modelUUser.Token = r.URL.Query().Get("token")
		modelUUser.Phone = r.URL.Query().Get("phone")
		modelUUser.Username = r.URL.Query().Get("name")
		modelUUser.Secondname = r.URL.Query().Get("surname")
		modelUUser.Middlname = r.URL.Query().Get("middlename")
		modelUUser.Password = r.URL.Query().Get("pwd")
		modelUUser.City = r.URL.Query().Get("city")
		modelUUser.Sex = r.URL.Query().Get("sex")
		modelUUser.DayB = r.URL.Query().Get("daybir")
		if modelUUser.Phone == "" && modelUUser.Username == "" && modelUUser.Middlname == "" && modelUUser.City == "" && modelUUser.DayB == "" {
			if modelUUser.Password != "" {
				UpdateOnlyPassword = true
			}
		}
		data, check, err := database.UpdateUserData(root.db, r, modelUUser)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(405)
		} else if check == 1 {
			w.WriteHeader(401)
		} else if UpdateOnlyPassword == true {
			w.WriteHeader(200)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		}
	}).Methods("PUT")
	//Обновлени/восстановление данных автомобиля.
	root.proc.router.HandleFunc("/auth/update/car", func(w http.ResponseWriter, r *http.Request) {
		modelUCar := model.SetCarData{}
		modelUCar.Token = r.URL.Query().Get("token")
		modelUCar.Color = r.URL.Query().Get("color")
		modelUCar.Mark = r.URL.Query().Get("mark")
		modelUCar.NumReg = r.URL.Query().Get("num")
		fbToken := r.URL.Query().Get("fb")
		data, check, err := database.UpdateCarData(root.db, r, modelUCar, fbToken)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(405)
		} else if check == 1 {
			w.WriteHeader(401)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		}
	}).Methods("PUT")
	//Удаление автомобиля
	root.proc.router.HandleFunc("/auth/car", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		res, check := database.DelCar(root.db, token)
		if res == 1 {
			w.WriteHeader(401)
		} else if check == false {
			w.WriteHeader(405)
		} else {
			w.WriteHeader(200)
		}
	}).Methods("DELETE")
	//Изображение было добавлено
	root.proc.router.HandleFunc("/auth/user/set-img", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		if len(token) != 162 {
			w.WriteHeader(415)
		} else {
			_, check := database.SetImage(root.db, token)
			if check != true {
				w.WriteHeader(520)
			} else {
				w.WriteHeader(200)
			}
		}
	}).Methods("PUT")
	//
	root.proc.router.HandleFunc("/auth/switch", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		check, err := database.SwitchStatusDriverOnUser(root.db, token)
		if check == 1 {
			w.WriteHeader(401)
		} else if err != nil {
			w.WriteHeader(405)
		} else {
			w.WriteHeader(200)
		}
	}).Methods("GET")
	//Изменение данных.
	root.proc.router.HandleFunc("/auth/user/delete-img", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		if len(token) != 162 {
			w.WriteHeader(415)
		} else {
			check := database.RemoveImage(root.db, token)
			if check != true {
				w.WriteHeader(520)
			} else {
				w.WriteHeader(200)
			}
		}
	}).Methods("DELETE")
	//Проверка смс.
	root.proc.router.HandleFunc("/auth/sms/check/{phone}/{key}", func(w http.ResponseWriter, r *http.Request) { //READY
		phone, _ := mux.Vars(r)["phone"]
		key, _ := mux.Vars(r)["key"]
		data, err := database.CheckSms(root.db, phone, key)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(405)
		}
		if data == nil {
			w.WriteHeader(405)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		}
	}).Methods("GET")
	//Запрос смс.
	root.proc.router.HandleFunc("/auth/sms/call", func(w http.ResponseWriter, r *http.Request) { //READY
		phone := r.URL.Query().Get("phone")
		err := database.CallSms(root.db, root.sms, phone)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(405)
		} else {
			w.WriteHeader(200)
		}
	}).Methods("GET")
	//Запрос смс.
	root.proc.router.HandleFunc("/auth/sms/get/{phone}", func(w http.ResponseWriter, r *http.Request) { //READY
		phone, _ := mux.Vars(r)["phone"]
		data, err := database.CallSmsDevel(root.db, phone)
		if err != nil {
			w.WriteHeader(405)
		} else {
			w.Header().Set("Content-Type", "application/json") //take sms user
			w.Write(data)
		}
	}).Methods("GET")
	//Проверка на существование пользователя в бд сервера.
	root.proc.router.HandleFunc("/auth/check/registration/{phone}", func(w http.ResponseWriter, r *http.Request) { //READY
		phone, _ := mux.Vars(r)["phone"]
		check := database.CheckingUser(root.db, phone)
		if check == false {
			w.WriteHeader(204)
		} else {
			w.WriteHeader(201)
		}
	}).Methods("GET")
	//Выгрузка сессий.
	root.proc.router.HandleFunc("/auth/list/session", func(w http.ResponseWriter, r *http.Request) { //READY
		token := r.URL.Query().Get("token")
		data, err := database.SessionsDriver(root.db, token)
		if err != nil {
			errCh <- err.Error()
			w.WriteHeader(405)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		}
	}).Methods("GET")
	//Авторизация.
	root.proc.router.HandleFunc("/auth/check/login", func(w http.ResponseWriter, r *http.Request) { //READY
		phone, pwd, _ := r.BasicAuth()
		if len(phone) == 10 && len(pwd) == 64 {
			fbToken := r.URL.Query().Get("fb")
			fmt.Println("User Data of login: ", phone, " and pass: ", pwd, "\nfb: ", fbToken)
			data, check, err := database.AuthUser(root.db, phone, pwd, fbToken)
			writeAuthorithationData(data, check, err, w, errCh)
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
	//Восстановление токена.
	root.proc.router.HandleFunc("/auth/check/auth", func(w http.ResponseWriter, r *http.Request) { //READY
		phone, pwd, _ := r.BasicAuth()
		if len(phone) == 10 && len(pwd) == 64 {
			fbToken := r.URL.Query().Get("fb")
			data, check, err := database.CheckAuth(root.db, phone, pwd, fbToken)
			writeAuthorithationData(data, check, err, w, errCh)
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
	//Регистрация водителя.
	root.proc.router.HandleFunc("/auth/registration/driver", func(w http.ResponseWriter, r *http.Request) {
		data, check, err := database.RegistrationDriver(root.db, r)
		writeDataWithCheckToken(data, check, err, w, errCh)
	}).Methods("POST")
	//Регистрация автомобиля.
	root.proc.router.HandleFunc("/auth/registration/car", func(w http.ResponseWriter, r *http.Request) {
		data, check, err := database.RegistrationCar(root.db, r)
		writeDataWithCheckToken(data, check, err, w, errCh)
	}).Methods("POST")
	//Регистрация пользователя.
	root.proc.router.HandleFunc("/auth/registration/user", func(w http.ResponseWriter, r *http.Request) {
		data, check, err := database.RegistrationUser(root.db, r)
		writeDataWithCheckToken(data, check, err, w, errCh)
	}).Methods("POST")
	//Регистрация партнера.
	root.proc.router.HandleFunc("/auth/registration/agent", func(w http.ResponseWriter, r *http.Request) {
		data, check, err := database.RegistrationAgent(root.db, r)
		writeDataWithCheckToken(data, check, err, w, errCh)
	}).Methods("POST")
	//Регистрация кабинета.
	root.proc.router.HandleFunc("/auth/registration/cabinet", func(w http.ResponseWriter, r *http.Request) {
		data, check, err := database.RegistrationCabinet(root.db, r)
		writeDataWithCheckToken(data, check, err, w, errCh)
	}).Methods("POST")
	//Список партнеров в городе.
	root.proc.router.HandleFunc("/auth/list/partners", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		if len(token) == 162 {
			data, err := database.ListCabinetInCity(root.db, token)
			if err != nil {
				w.WriteHeader(204)
			} else {
				w.Header().Set("Content-Type", "application/json") //take sms user
				w.Write(data)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
	//Список предложенных автомобилей.
	root.proc.router.HandleFunc("/auth/rend/car", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		if len(token) == 162 {
			data, err := database.CheckRendAuto(root.db, token)
			if err != nil {
				w.WriteHeader(204)
			} else if data == nil {
				w.WriteHeader(204)
			} else {
				w.Header().Set("Content-Type", "application/json") //take sms user
				w.Write(data)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
	//Информация о сессиях клиента
	root.proc.router.HandleFunc("/auth/session", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		if len(token) == 162 {
			data, err := database.SessionInformation(root.db, token)
			if err != nil {
				errCh <- err.Error()
				w.WriteHeader(204)
			} else if data == nil {
				w.WriteHeader(204)
			} else {
				w.Header().Set("Content-Type", "application/json") //take sms user
				w.Write(data)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
	//Взять автомобиль в аренду
	root.proc.router.HandleFunc("/auth/rend/car", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		idCar := r.URL.Query().Get("car")
		idCabinet := r.URL.Query().Get("cabinet")
		if len(token) == 162 {
			data, check, err := database.KeepAuto(root.db, token, idCar, idCabinet)
			if err != nil {
				w.WriteHeader(405)
			} else if !check {
				w.WriteHeader(405)
			} else {
				w.Header().Set("Content-Type", "application/json") //take sms user
				w.Write(data)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("POST")
	//Вернуть арендованный автомобиль
	root.proc.router.HandleFunc("/auth/rend/car", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		if len(token) == 162 {
			data, check, err := database.KeepOutAuto(root.db, token)
			if err != nil {
				w.WriteHeader(405)
			} else if !check {
				w.WriteHeader(405)
			} else {
				w.Header().Set("Content-Type", "application/json") //take sms user
				w.Write(data)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("DELETE")
	/************F*O*R***S*E*R*V*E*R***A*N*D***P*A*R*T*N*E*R************/
	root.proc.router.HandleFunc("/image", func(w http.ResponseWriter, r *http.Request) {
		token := r.URL.Query().Get("token")
		if len(token) == 162 {
			fmt.Println("Check token for set image")
			data, check := database.SetImage(root.db, token)
			if check != true {
				w.WriteHeader(401)
			} else {
				w.Header().Set("Content-Type", "application/json")
				w.Write(data)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
	//Фиксация данных, добавление токена оплаты
	root.proc.router.HandleFunc("/auth/pay/payment", func(w http.ResponseWriter, r *http.Request) {
		data, check, err := database.YandexPay(root.db, r)
		if check == 3 {
			w.WriteHeader(401) //error
		} else if check == 0 && err == nil {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		} else if err != nil {
			w.WriteHeader(405)
			errCh <- err.Error()
		} else {
			w.WriteHeader(405)
		}
	}).Methods("POST")
	//Регистрация оплаты, требуется для проверки данных оплаты
	root.proc.router.HandleFunc("/auth/pay/registration", func(w http.ResponseWriter, r *http.Request) {
		data, err := database.CreatePay(root.db, r)
		if err != nil {
			w.WriteHeader(401)
			errCh <- err.Error()
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		}
	}).Methods("POST")
	//Регистрация изменения статуса оплаты
	root.proc.router.HandleFunc("/auth/pay/status", func(w http.ResponseWriter, r *http.Request) {
		err := database.StatusPay(root.db, r)
		if err != nil {
			w.WriteHeader(401)
			errCh <- err.Error()
		} else {
			w.WriteHeader(200)
		}
	}).Methods("POST")
	//Авторизация
	root.proc.router.HandleFunc("/auth/sad/check/login", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		phone, pwd, _ := r.BasicAuth()
		fmt.Println(phone, pwd)
		if len(pwd) > 60 && len(pwd) < 120 {
			fmt.Println("Check admin data")
			data, check, err := database.AuthAdmin(root.db, phone, pwd, "")
			writeAnswerSystemHandlersWithData(check, data, err, w, errCh)
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
	root.proc.router.HandleFunc("/auth/sad/check/login", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
	}).Methods("OPTIONS")
	//list users
	root.proc.router.HandleFunc("/auth/sad/users", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		token := r.URL.Query().Get("token")
		param := r.URL.Query().Get("param")
		if len(token) == 162 {
			err := database.CheckAdministratorToken(root.db, token)
			if err == nil {
				switch param {
				case "all":
					{
						data, err := database.ListUserForSystem(root.db, true, false, false)
						if err != nil {
							w.WriteHeader(405)
						} else {
							w.Header().Set("Content-Type", "application/json")
							w.Write(data)
						}
					}
				case "driver":
					{
						data, err := database.ListUserForSystem(root.db, false, true, true)
						if err != nil {
							w.WriteHeader(405)
						} else {
							w.Header().Set("Content-Type", "application/json")
							w.Write(data)
						}
					}
				case "no-ver":
					{
						data, err := database.ListUserForSystem(root.db, false, true, false)
						if err != nil {
							w.WriteHeader(405)
						} else {
							w.Header().Set("Content-Type", "application/json")
							w.Write(data)
						}
					}
				case "online":
					{
						w.WriteHeader(5)
					}
				default:
					{
						w.WriteHeader(405)
					}
				}
			} else {
				w.WriteHeader(401)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
	//Verification driver
	root.proc.router.HandleFunc("/auth/sad/verification/user", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		token := r.URL.Query().Get("token")
		user := r.URL.Query().Get("user")
		param := r.URL.Query().Get("param")
		if len(token) == 162 {
			err := database.CheckAdministratorToken(root.db, token)
			if err == nil {
				if param == "t" {
					if err := database.VerificationForDriver(root.db, user, true); err != nil {
						w.WriteHeader(405)
					} else {
						w.WriteHeader(200)
					}
				} else if param == "f" {
					if err := database.VerificationForDriver(root.db, user, false); err != nil {
						w.WriteHeader(405)
					} else {
						w.WriteHeader(200)
					}
				} else {
					w.WriteHeader(405)
				}
			} else {
				w.WriteHeader(401)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("POST")
	root.proc.router.HandleFunc("/auth/sad/users", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
	}).Methods("OPTIONS")
	//list users
	root.proc.router.HandleFunc("/auth/sad/cars", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		token := r.URL.Query().Get("token")
		param := r.URL.Query().Get("param")
		if len(token) == 162 {
			err := database.CheckAdministratorToken(root.db, token)
			if err == nil {
				switch param {
				case "all":
					{
						data, err := database.ListCarsForSystem(root.db, true, false)
						if err != nil {
							w.WriteHeader(405)
						} else {
							w.Header().Set("Content-Type", "application/json")
							w.Write(data)
						}
					}
				case "no-ver":
					{
						data, err := database.ListCarsForSystem(root.db, false, true)
						if err != nil {
							w.WriteHeader(405)
						} else {
							w.Header().Set("Content-Type", "application/json")
							w.Write(data)
						}
					}
				default:
					{
						w.WriteHeader(405)
					}
				}
			} else {
				w.WriteHeader(401)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
	//Verification car
	root.proc.router.HandleFunc("/auth/sad/verification/cars", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		token := r.URL.Query().Get("token")
		user := r.URL.Query().Get("user")
		param := r.URL.Query().Get("param")
		if len(token) == 162 {
			err := database.CheckAdministratorToken(root.db, token)
			if err == nil {
				if param == "t" {
					if err := database.VerificationForCar(root.db, user, true); err != nil {
						w.WriteHeader(405)
					} else {
						w.WriteHeader(200)
					}
				} else if param == "f" {
					if err := database.VerificationForCar(root.db, user, false); err != nil {
						w.WriteHeader(405)
					} else {
						w.WriteHeader(200)
					}
				} else {
					w.WriteHeader(405)
				}
			} else {
				w.WriteHeader(401)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("POST")
	root.proc.router.HandleFunc("/auth/sad/cars", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
	}).Methods("OPTIONS")
	//Проверка токена
	root.proc.router.HandleFunc("/auth/sad/check/token", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		token := r.URL.Query().Get("token")
		if len(token) == 162 {
			fmt.Println("Check admin data")
			check, err := database.CheckTokenAdmin(root.db, token)
			if check == 0 && err == nil {
				w.WriteHeader(200)
			} else {
				w.WriteHeader(401)
			}
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
	root.proc.router.HandleFunc("/auth/sad/check/token", func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
	}).Methods("OPTIONS")
	//
	root.proc.router.HandleFunc("/auth/sps/check/login", func(w http.ResponseWriter, r *http.Request) {
		phone, pwd, _ := r.BasicAuth()
		if len(pwd) < 64 || len(pwd) > 120 {
			data, check, err := database.AuthAgent(root.db, phone, pwd, "")
			writeAnswerSystemHandlersWithData(check, data, err, w, errCh)
		} else {
			w.WriteHeader(401)
		}
	}).Methods("GET")
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "GET")
	(*w).Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
}

func writeDataWithCheckToken(data []byte, check int, err error, w http.ResponseWriter, errCh chan<- string) {
	if check == 1 || check == 4 {
		w.WriteHeader(201) //exist
	} else if check == 2 {
		w.WriteHeader(520) //error
	} else if check == 3 {
		w.WriteHeader(401) //error
	} else if err != nil {
		w.WriteHeader(405)
		errCh <- err.Error()
	} else if data != nil && check == 0 {
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}
}

func writeStandartData(data []byte, err error, w http.ResponseWriter, errCh chan<- string) {
	if err != nil {
		errCh <- err.Error()
		w.WriteHeader(401)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}
}

func writeAuthorithationData(data []byte, check int, err error, w http.ResponseWriter, errCh chan<- string) {
	if err != nil {
		errCh <- err.Error()
		w.WriteHeader(405)
	}
	if check == 1 {
		w.WriteHeader(401)
	} else if check == 2 {
		w.WriteHeader(423)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}
}

func writeAnswerSystemHandlersWithData(check int, data []byte, err error, w http.ResponseWriter, errCh chan<- string) {
	if check == 1 {
		w.WriteHeader(401)
	} else if check == 2 {
		w.WriteHeader(201)
	} else if check == 3 {
		w.WriteHeader(523)
	} else if err != nil {
		errCh <- err.Error()
		w.WriteHeader(405)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}
}
