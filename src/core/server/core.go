package server

import (
	"../../data/database"

	"../../sms_server"
	"../../utility"
	"../scar"
)

var errorCore = "Core module error#"

var core Core

//Core - is a storage structure for Core units
type Core struct {
	conf *utility.Configuration
	proc *HTTPProcess
	db   *database.Database
	sc   *scar.SCAR
	sms  *sms_server.SMS
}

//InitCore - initialization core
func InitCore(path string) error {
	conf, err := utility.LoadFile(path) //Read config file
	if err != nil {
		return err
	}
	if !conf.IsValid() {
		return utility.ErrorCreate(errorCore, "InitCore.- In configuration file attend empty fields... ")
	}
	// scar, err := scar.SynchWithScar(conf.ScarAddrServer, conf.ScarCrt, conf.ScarKey, conf.ScarID)
	// if err != nil {
	// 	return utility.ErrorCreate(errorCore, ".InitCore.SynchWithScar.")
	// }
	core = Core{conf, InitServer(), database.DBInstance(conf.ConnectStrPg), &scar.SCAR{}, sms_server.InitSmsReservers()}
	return nil
}

//Start - starting core
func Start(err chan<- string) error {
	if errDb := core.db.InitDatabaseConnections(); errDb != nil {
		err <- errDb.Error()
		return errDb
	}
	core.Handlers(err)
	// go core.db.BusHandlersOrderAction(err)
	go core.db.BusHandlersVerificationAction(err)
	go core.db.PayStatusHandler()
	// go core.db.StartWorkers()
	//Test requests
	// if errSc := core.sc.KeepSettingForNetwork(); errSc != nil {
	// 	err <- errSc.Error()
	// 	return errSc
	// }
	// go core.sc.ControlSynchronizationDataOfServerOnServerOfSCAR(err)
	//If It is execute them the http server start
	if errProc := core.proc.StartProcess(core.conf); errProc != nil {
		err <- errProc.Error()
		return errProc
	}
	return nil
}

//DestructCore - destruct core
// func DestructCore() error {
// 	return core.proc.server.Shutdown()
// }
